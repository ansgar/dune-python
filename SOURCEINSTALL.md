You can build dune-corepy like any other DUNE module -
the general Dune installation guide can be found [here][installation].

As we are building Python extension modules, all Dune libraries need to be
build as *position independent code*, so that they can be linked into a shared
library.
This is most easily achieved by building shared libraries in all Dune modules,
i.e., by adding `-DBUILD_SHARED_LIBS=TRUE` to your `CMAKE_FLAGS`.
Alternatively, you can add `-DCMAKE_POSITION_INDEPENDENT_CODE=TRUE` to the
`CMAKE_FLAGS`, forcing all code to be built in a position independent manner.
Some of the Python bindings need to access implementation-dependent extensions
of the Dune grid interface. We therefore recommend to enable experimental grid
extensions, i.e., to add `-DDUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS=TRUE` to
your `CMAKE_FLAGS`, as well.

The python binding are installed independently from the actual dune modules. So
for example it is possible to not install any Dune module but have the Dune python
package installed in the local *site-directory*.

To install the python package, you need to specify the desired location.
Python modules can either be installed system-wide or
on a per user basis. This can be specified by adding
`-DDUNE_PYTHON_INSTALL_LOCATION=system` or `-DDUNE_PYTHON_INSTALL_LOCATION=user` to your
`CMAKE_FLAGS`. Note that for installation into a python virtual env
`system` should be used.

So the simplest `CMAKE_FLAGS` set for example in the option file passed to `dunecontrol`
would be:
~~~
CMAKE_FLAGS="-DBUILD_SHARED_LIBS=TRUE \
             -DDUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS=TRUE \
             -DPYTHON_INSTALL_LOCATION="system"
            "
~~~

To install the Dune python package the simplest approach is to execute
~~~
dune-corepy/bin/setup-dunepy.py --opts=config.opts install
~~~
This will construct the `dune-py` module in the location pointed to by 
`DUNE_PY_DIR` (default `~/.cache`) and install the python bindings
of all dune modules depending on or suggesting dune-corepy.

To install the Dune bindings for a given module individually, a new cmake target
`install_python` is created in the case that
`DUNE_PYTHON_INSTALL_LOCATION` is set to either `system` or `user`.
To perform the actual installation `make install_python` needs to be called in the build
directory of each dune module with python bindings.
Alternatively, you can call `pip install` or `pip install --user` in
the `python` subdirectory of the build directory of each Dune module.
If you don't want to install the Dune python package you need to make sure that
your `PYTHONPATH` contains the paths to the build directories of all modules with
python binding.

If you wish to build the sphinx documentation, enter the build directory of `dune-corepy` and
call
```
make doc
```
You can read it by opening ./doc/sphinx/html/index.html with your browser. You
will find notes on how to run the Python demonstrations, a short manual for
users and an other manual for developers who may be interested in contributing
to dune-corepy or simply learn more about the technical details.

[installation]: https://dune-project.org/doc/installation
