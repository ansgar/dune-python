// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_COREPY_COMMON_FMATRIX_HH
#define DUNE_COREPY_COMMON_FMATRIX_HH

#include <string>
#include <tuple>

#include <dune/common/fmatrix.hh>
#include <dune/common/std/utility.hh>

#include <dune/corepy/common/densematrix.hh>
#include <dune/corepy/pybind11/pybind11.h>
#include <dune/corepy/pybind11/operators.h>

namespace Dune
{

  namespace CorePy
  {

    namespace py = pybind11;

    template<class K, int rows, int cols>
    static void registerFieldMatrix(py::handle scope)
    {
      typedef Dune::FieldMatrix<K, rows, cols> FM;

      static const std::string clsName = "FieldMatrix" + std::to_string(rows) + std::to_string(cols);
      py::class_<FM> cls(scope, clsName.c_str(), py::buffer_protocol() );

      cls.def(py::init<>());

      cls.def("__init__",
          [] (FM& m, py::list l) {
            using std::size_t;

            new(&m) FM(K(0));

            size_t l_rows = l.size();
            for (size_t r = 0; r < l_rows; r++)
            {
              size_t l_cols = l[r].cast<py::list>().size();
              for (size_t c = 0; c < l_cols; c++)
                m[r][c] = l[r].cast<py::list>()[c].cast<K>();
            }
          });

      cls.def("__repr__",
          [] (const FM& m) {
            std::string repr = "DUNE FieldMatrix:\n(";

            for(unsigned int r = 0; r < rows; r++)
            {
              repr += "(";
              for (unsigned int c = 0; c < cols; c++)
                repr += (c > 0 ? ", " : "") + std::to_string(m[r][c]);
              repr += std::string(")") + ((int)r < rows - 1 ? "\n" : "");
            }

            repr += ")";

            return repr;
          });


      cls.def_buffer([] (FM& m) -> py::buffer_info {
          return py::buffer_info(
              &(*m.begin()),                   /* Pointer to buffer */
              sizeof(K),                       /* Size of one scalar */
              py::format_descriptor<K>::value, /* Python struct-style format descriptor */
              2,                               /* Number of dimensions */
              { rows, cols },                  /* Buffer dimensions */
              { sizeof(K) * rows,              /* Strides (in bytes) for each index */
                sizeof(K) }
          );
      });

      registerDenseMatrix<FM>(cls);
    }

    template<class K, int rows, int... cols>
    static void registerCols(py::handle scope, std::integer_sequence<int, cols...>)
    {
      std::ignore = std::make_tuple((registerFieldMatrix<K, rows, cols>(scope), 0)...);
    }

    template<class K, int... rows>
    static void registerFieldMatrix(py::handle scope, std::integer_sequence<int, rows...> seq)
    {
      std::ignore = std::make_tuple((registerCols<K, rows>(scope, seq), 0)...);
    }

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_COMMON_FMATRIX_HH
