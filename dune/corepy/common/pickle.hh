#ifndef DUNE_COREPY_COMMON_PICKLE_HH
#define DUNE_COREPY_COMMON_PICKLE_HH

#include <type_traits>
#include <utility>

#include <dune/common/documentation.hh>
#include <dune/common/typeutilities.hh>

#include <dune/corepy/pybind11/pybind11.h>

namespace Dune
{

  namespace CorePy
  {

    // Pickler
    // -------

    template< class T, class SFINAE = void >
    struct Pickler;

#ifdef DOXYGEN
    /**
     * \brief Pickling support for a C++ type
     *
     * Specialize this class to add pickling support to interface classes
     * supporting it.
     **/
    template< class T >
    struct Pickler< T, void >
    {
      static ImplementationDefined getState ( const T &self );
      static void setState ( T &self, const ImplementationDefined &data );
    };
#endif // #ifdef DOCYGEN



    // HasPickler
    // ----------

    namespace detail
    {

      template< class T >
      decltype( Pickler< T >::getState( std::declval< const T & >() ), std::true_type() ) hasPickler ( const T &, PriorityTag< 1 > );

      template< class T >
      std::false_type hasPickler ( const T &, PriorityTag< 0 > );

    } // namespace detail

    template< class T >
    struct HasPickler
      : public decltype( detail::hasPickler( std::declval< const T & >(), PriorityTag< 1 >() ) )
    {};



    // registerPickleSupport
    // ---------------------

    template< class T, class... options >
    inline static std::enable_if_t< HasPickler< T >::value > registerPickleSupport ( pybind11::class_< T, options... > cls )
    {
      cls.def( "__getstate__", Pickler< T >::getState );
      cls.def( "__setstate__", Pickler< T >::setState );
    }

    template< class T, class... options >
    inline static std::enable_if_t< !HasPickler< T >::value > registerPickleSupport ( pybind11::class_< T, options... > cls )
    {
      cls.def( "__getstate__", [] ( const T & ) -> pybind11::object { throw std::invalid_argument( "Pickling not supported for " + className< T > () + "."); } );
      cls.def( "__setstate__", [] ( const T &, pybind11::object ) { throw std::invalid_argument( "Pickling not supported for " + className< T > () + "."); } );
    }

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_COMMON_PICKLE_HH
