// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_COREPY_COMMON_FVECTOR_HH
#define DUNE_COREPY_COMMON_FVECTOR_HH

#include <cstddef>

#include <stdexcept>
#include <string>
#include <tuple>

#include <dune/common/fvector.hh>
#include <dune/common/std/utility.hh>

#include <dune/corepy/common/densevector.hh>
#include <dune/corepy/pybind11/pybind11.h>
#include <dune/corepy/pybind11/operators.h>

namespace Dune
{

  namespace CorePy
  {

    namespace py = pybind11;

    template< class K, int size >
    void registerFieldVector ( py::handle scope, std::integral_constant< int, size > = {} )
    {
      typedef Dune::FieldVector<K, size> FV;

      static const std::string clsName = "FieldVector" + std::to_string(size);
      py::class_<FV> cls(scope, clsName.c_str());

      cls.def(py::init<>());

      cls.def( "__init__", [] ( FV &self, pybind11::buffer other ) {
          pybind11::buffer_info info = other.request();
          if( info.format != pybind11::format_descriptor< K >::format() )
            throw std::runtime_error( "Incompatible buffer format." );
          if( info.ndim != 1 )
            throw std::runtime_error( "Only on-dimensional buffers can be converted into FieldVector." );
          const std::size_t stride = info.strides[ 0 ] / sizeof( K );
          const std::size_t sz = std::min( self.size(), info.shape[ 0 ] );
          for( std::size_t i = 0; i < sz; ++i )
            self[ i ] = static_cast< K * >( info.ptr )[ i*stride ];
        } );

      cls.def( "__init__", [] ( FV &self, pybind11::tuple t ) {
          new (&self) FV( K( 0 ) );
          const std::size_t sz = std::min( self.size(), t.size() );
          // should this fail in case the sizes do not match?
          for( std::size_t i = 0; i < sz; ++i )
            self[ i ] = t[ i ].template cast< K >();
        } );

      cls.def("__init__",
          [] (FV& v, py::list l) {
            new(&v) FV(K(0));
            const std::size_t sz = std::min(v.size(), l.size());
            // should this fail in case the sizes do not match?
            for (std::size_t i = 0; i < sz; ++i)
              v[i] = l[i].template cast<K>();
          });
      cls.def("__init__",
          [] (FV& v, py::args l) {
            new(&v) FV(K(0));
            const std::size_t sz = std::min(v.size(), l.size());
            // should this fail in case the sizes do not match?
            for (std::size_t i = 0; i < sz; ++i)
              v[i] = l[i].template cast<K>();
          });

      py::implicitly_convertible<py::args,FV>();
      py::implicitly_convertible<py::buffer, FV>();

      cls.def("copy", [](FV& , py::args l) {
            FV v(K(0));
            const std::size_t sz = std::min(v.size(), l.size());
            // should this fail in case the sizes do not match?
            for (std::size_t i = 0; i < sz; ++i)
              v[i] = l[i].template cast<K>();
            return v;
          });


      cls.def("__repr__",
          [] (const FV &v) {
            std::string repr = "DUNE FieldVector: (";

            for (std::size_t i = 0; i < v.size(); ++i)
              repr += (i > 0 ? ", " : "") + std::to_string(v[i]);

            repr += ")";

            return repr;
          });

      registerDenseVector<FV>(cls);

      if (size == 1)
      {
        cls.def("__float__", [] (FV &v) { return v[0]; } );
        py::implicitly_convertible<double,FV>();
        cls.def("__init__", [] (FV &v, double a) { new (&v) FV(a); } );
      }
    }

    template<class K, int... size>
    void registerFieldVector(py::handle scope, std::integer_sequence<int, size...>)
    {
      std::ignore = std::make_tuple((registerFieldVector<K>(scope, std::integral_constant<int, size>()), size)...);
    }

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_COMMON_FVECTOR_HH
