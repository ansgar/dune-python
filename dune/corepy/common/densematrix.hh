// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_COREPY_COMMON_DENSEMATRIX_HH
#define DUNE_COREPY_COMMON_DENSEMATRIX_HH

#include <string>
#include <tuple>

#include <dune/common/std/utility.hh>

#include <dune/corepy/pybind11/pybind11.h>
#include <dune/corepy/pybind11/operators.h>

namespace Dune
{

  namespace CorePy
  {

    namespace py = pybind11;

    // registerDenseMatrix
    //
    // DerivedType is FieldMatrix or DynamicMatrix
    // -------------------------------------------

    template<typename DerivedType>
    void registerDenseMatrix(py::class_<DerivedType>& cls)
    {
      using ValueType = typename DerivedType::value_type;
      using RowType   = typename DerivedType::row_type;
      using RowRef    = typename DerivedType::row_reference;

      cls.def("__getitem__",
          [] (DerivedType& m, std::size_t i) -> RowRef {
            if (i < m.mat_rows())
              return m[i];
            else
              throw py::index_error();
          },
          py::return_value_policy::reference_internal);

      cls.def("__setitem__",
          [] (DerivedType& m, std::size_t i, py::object l) {
            if (i < m.mat_rows())
            {
              RowType v = l.cast<RowType>();
              std::size_t size = std::min(m.mat_cols(), v.size());

              for (std::size_t j = 0; j < size; j++)
                m[i][j] = v[j];
            }
            else
              throw py::index_error();
          });

      cls.def("__len__", [] (const DerivedType& m) -> std::size_t { return m.size(); });
      cls.def("invert", &DerivedType::invert);

      cls.def(py::self += py::self);
      cls.def(py::self -= py::self);
      cls.def(py::self *= ValueType());
      cls.def(py::self /= ValueType());

      cls.def(py::self == py::self);
      cls.def(py::self != py::self);

      cls.def_property_readonly("frobenius_norm",     [](const DerivedType& m) { return m.frobenius_norm(); });
      cls.def_property_readonly("frobenius_norm2",    [](const DerivedType& m) { return m.frobenius_norm2(); });
      cls.def_property_readonly("infinity_norm",      [](const DerivedType& m) { return m.infinity_norm(); });
      cls.def_property_readonly("infinity_norm_real", [](const DerivedType& m) { return m.infinity_norm_real(); });
      cls.def_property_readonly("rows", [](const DerivedType& m) { return m.mat_rows(); });
      cls.def_property_readonly("cols", [](const DerivedType& m) { return m.mat_cols(); });

      py::implicitly_convertible<py::list, DerivedType>();
    }

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_COMMON_DENSEMATRIX_HH
