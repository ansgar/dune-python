#ifndef DUNE_COREPY_COMMON_TYPEREGISTRY_HH
#define DUNE_COREPY_COMMON_TYPEREGISTRY_HH

#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <string>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>
#include <utility>
#include <vector>

#include <dune/corepy/pybind11/pybind11.h>

namespace Dune
{

  namespace CorePy
  {

    // TypeRegistry
    // ------------

    struct TypeRegistry
    {
      struct Entry
      {
        std::string name;
        std::vector< std::string > includes;
        pybind11::object object;
      };

    private:
      typedef std::unordered_map< std::type_index, Entry > Entries;

      TypeRegistry () = default;

    public:
      typedef Entries::iterator iterator;
      typedef Entries::const_iterator const_iterator;
      typedef std::pair< iterator, bool > RetInsert;

      static TypeRegistry &instance ()
      {
        static TypeRegistry registry_;
        return registry_;
      }

    protected:
      // The following two functions should be implemented in the lib

      const Entry &at ( const std::type_index &typeId ) const
      {
        auto pos = entries_.find( typeId );
        assert( pos != entries_.end() );
        return pos->second;
      }

      Entry &at ( const std::type_index &typeId )
      {
        auto pos = entries_.find( typeId );
        assert( pos != entries_.end() );
        return pos->second;
      }

      const_iterator find ( const std::type_index &typeId ) const
      {
        return entries_.find( typeId );
      }

      /**
       * \brief register a C++ type
       *
       * \param[in]  typeId    type index of the C++ type
       * \param[in]  name      full name of the C++ type (including template arguments, if applicable)
       * \param[in]  includes  list of necessary includes to use this type
       **/
      RetInsert insert ( const std::type_index &typeId, const std::string &name, std::vector< std::string > includes )
      {
        RetInsert ret = entries_.insert( std::make_pair( std::type_index( typeId ), Entry() ) );
        if( ret.second )
        {
          Entry &entry = ret.first->second;
          entry.name = name;
          entry.includes = std::move( includes );
          finalize( entry );
        }
        return ret;
      }

      /**
       * \brief register a C++ template
       *
       * \param[in]  name      name of the C++ template
       * \param[in]  includes  list of necessary includes to use this template
       * \param[in]  args      list of template arguments
       **/
      RetInsert insertTemplate ( const std::type_index &typeId, const std::string &name, const std::vector< std::string > &includes, const std::vector< Entry > &args )
      {
        std::string fullName = name;
        std::vector< std::string > fullIncludes = includes;

        if( !args.empty() )
        {
          const char *delim = "< ";
          for( const Entry &arg : args )
          {
            fullName += delim + arg.name;
            fullIncludes.insert( fullIncludes.end(), arg.includes.begin(), arg.includes.end() );
            delim = ", ";
          }
          fullName += " >";
        }
        else
          fullName += "<>";

        return insert( typeId, fullName, fullIncludes );
      }

    public:
      template< class T >
      const Entry &at () const
      {
        return at( typeid( T ) );
      }

      template< class T >
      Entry &at ()
      {
        return at( typeid( T ) );
      }

      template< class T >
      const_iterator find () const
      {
        return find( typeid( T ) );
      }

      const_iterator begin () const { return entries_.begin(); }
      const_iterator end () const { return entries_.end(); }

      /**
       * \brief register a C++ type
       *
       * \tparam  T  C++ type to register
       *
       * \param[in]  name      full name of the C++ type (including template arguments, if applicable)
       * \param[in]  includes  list of necessary includes to use this type
       **/
      template< class T >
      RetInsert insert ( const std::string &name, std::vector< std::string > includes = {} )
      {
        return insert( typeid( T ), name, std::move( includes ) );
      }

      /**
       * \brief register a C++ template
       *
       * \param[in]  name      name of the C++ template
       * \param[in]  includes  list of necessary includes to use this template
       * \param[in]  args      either template arguement name (std::string) or entry (Entry)
       **/
      template< class T, class ... Args >
      RetInsert insert ( const std::string &name, std::vector< std::string > includes, Args &&... args )
      {
        return insertTemplate( typeid( T ), name, std::move( includes ), { getEntry( std::forward< Args >( args ) )... } );
      }

      /**
       * \brief register a C++ template with template argument deduction
       *
       * \tparam  useFirstArgs  ??? (first 'useFirstArgs many template arguments)
       *
       * \param[in]  name      name of the C++ template
       * \param[in]  includes  list of necessary includes to use this template
       **/
      template< class T, int useFirstArgs >
      RetInsert insert ( const std::string &name, std::vector< std::string > includes = {} )
      {
        return insertTemplate( typeid( T ), name, std::move( includes ), Extract< T >::entries( *this ) );
      }

      /**
       * \brief register a nested C++ type
       *
       * \tparam  Outer  outer C++ type
       * \tparam  Inner  inner C++ type
       *
       * \param[in]  name      full name of the inner C++ type (including template arguments, if applicable)
       * \param[in]  includes  list of necessary includes to use the inner type
       **/
      template< class Outer, class Inner >
      RetInsert insertInner ( const std::string &name, std::vector< std::string > includes = {} )
      {
        RetInsert ret = insert( typeid( Inner ), name, includes );
        if( ret.second )
          addOuterClass( typeid( Outer ), ret.first->second );
        return ret;
      }

      /**
       * \brief register a nested C++ template
       *
       * \tparam  Outer  outer C++ type
       * \tparam  Inner  inner C++ type
       *
       * \param[in]  name      name of the inner C++ template
       * \param[in]  includes  list of necessary includes to use the inner template
       * \param[in]  args      either template arguement name (std::string) or entry (Entry)
       **/
      template< class Outer, class Inner, class... Args >
      RetInsert insertInner ( const std::string &name, std::vector< std::string > includes, Args &&... args )
      {
        RetInsert ret = insert< Inner, Args ... >( name, std::move( includes ), std::forward< Args >( args )... );
        if( ret.second )
          addOuterClass( typeid( Outer ), ret.first->second );
        return ret;
      }

      /**
       * \brief register a nested C++ template
       *
       * \tparam  Outer  outer C++ type
       * \tparam  Inner  inner C++ type
       * \tparam  useFirstArgs  ??? (first 'useFirstArgs many template arguments)
       *
       * \param[in]  name      name of the C++ template
       * \param[in]  includes  list of necessary includes to use this template
       **/
      template< class Outer, class Inner, int useFirstArgs >
      RetInsert insertInner ( const std::string &name, std::vector< std::string > includes = {} )
      {
        auto ret = insert< Inner, useFirstArgs >( name, std::move( includes ) );
        if( ret.second )
          addOuterClass( typeid( Outer ), ret.first->second );
        return ret;
      }

      template< class Scope >
      void exportToPython ( Scope &scope, Entry &entry )
      {
        entry.object = scope;
        // attach the typeName and the includes both to each class instance ...
        scope.def_property_readonly( "_typeName", [ entry ] ( pybind11::object ) { return entry.name; } );
        scope.def_property_readonly( "_includes", [ entry ] ( pybind11::object ) { return entry.includes; } );
        // ... and also to each class
        scope.def_property_readonly_static( "_typeName", [ entry ] ( pybind11::object ) { return entry.name; } );
        scope.def_property_readonly_static( "_includes", [ entry ] ( pybind11::object ) { return entry.includes; } );
      }

    private:
      template< class X >
      struct Extract
      {
        static std::vector< Entry > entries ( const TypeRegistry &registry ) { return {}; }
      };

      template< template< class... > class X, class... Args >
      struct Extract< X< Args ... > >
      {
        static std::vector< Entry > entries ( const TypeRegistry &registry ) { return { registry.at< Args >()... }; }
      };

      Entry getEntry ( std::string name ) { return Entry{ name }; }
      Entry getEntry ( const Entry &entry ) { return entry; }

      void addOuterClass ( const std::type_index &typeId, Entry &entry ) const
      {
        const Entry &outer = at( typeId );
        entry.name = outer.name + "::" + entry.name;
        entry.includes.insert( entry.includes.end(), outer.includes.begin(), outer.includes.end() );
        finalize( entry );
      }

      static void finalize ( Entry &entry )
      {
        std::sort( entry.includes.begin(), entry.includes.end() );
        entry.includes.erase( std::unique( entry.includes.begin(), entry.includes.end() ), entry.includes.end() );
      }

      Entries entries_;
    };



    // typeRegistry
    // ------------

    TypeRegistry &typeRegistry () { return TypeRegistry::instance(); }

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_COMMON_TYPEREGISTRY_HH
