// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_COREPY_COMMON_DYNVECTOR_HH
#define DUNE_COREPY_COMMON_DYNVECTOR_HH

#include <string>
#include <tuple>

#include <dune/common/dynvector.hh>
#include <dune/common/std/utility.hh>

#include <dune/corepy/common/densevector.hh>
#include <dune/corepy/pybind11/pybind11.h>
#include <dune/corepy/pybind11/operators.h>

namespace Dune
{

  namespace CorePy
  {

    namespace py = pybind11;

    template<class K>
    void registerDynamicVector(py::handle scope)
    {
      typedef Dune::DynamicVector<K> DV;

      py::class_<DV> cls(scope, "DynamicVector");

      cls.def(py::init<>());

      cls.def("__init__",
          [] (DV& v, py::list l) {
            new(&v) DV(l.size(), K(0));

            for (std::size_t i = 0; i < l.size(); ++i)
              v[i] = l[i].template cast<K>();
          });

      cls.def("__repr__",
          [] (const DV &v) {
            std::string repr = "DUNE DynamicVector: (";

            for (std::size_t i = 0; i < v.size(); ++i)
              repr += (i > 0 ? ", " : "") + std::to_string(v[i]);

            repr += ")";

            return repr;
          });

      registerDenseVector<DV>(cls);
    }

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_COMMON_DYNVECTOR_HH
