// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_COREPY_COMMON_DYNMATRIX_HH
#define DUNE_COREPY_COMMON_DYNMATRIX_HH

#include <string>
#include <tuple>

#include <dune/common/dynmatrix.hh>
#include <dune/common/std/utility.hh>

#include <dune/corepy/common/densematrix.hh>
#include <dune/corepy/pybind11/pybind11.h>
#include <dune/corepy/pybind11/operators.h>

namespace Dune
{

  namespace CorePy
  {

    namespace py = pybind11;

    template<class K>
    static void registerDynamicMatrix(py::handle scope)
    {
      typedef Dune::DynamicMatrix<K> DM;

      py::class_<DM> cls(scope, "DynamicMatrix");

      cls.def(py::init<>());

      cls.def("__init__",
          [] (DM& m, py::list l) {
            using std::size_t;

            // TODO: what if l[i].size() < l[0].size() ? ... problem

            size_t rows = l.size();
            size_t cols = (rows > 0) ? l[0].cast<py::list>().size() : 0;

            new(&m) DM(rows, cols, K(0));

            for (size_t r = 0; r < rows; r++)
              for (size_t c = 0; c < cols; c++)
                m[r][c] = l[r].cast<py::list>()[c].cast<K>();
          });

      cls.def("__repr__",
          [] (const DM& m) {
            std::string repr = "DUNE DynamicMatrix:\n(";

            for(unsigned int r = 0; r < m.rows(); r++)
            {
              repr += "(";
              for (unsigned int c = 0; c < m.cols(); c++)
                repr += (c > 0 ? ", " : "") + std::to_string(m[r][c]);
              repr += std::string(")") + (r < m.rows() - 1 ? "\n" : "");
            }

            repr += ")";

            return repr;
          });

      registerDenseMatrix<DM>(cls);
    }

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_COMMON_DYNMATRIX_HH
