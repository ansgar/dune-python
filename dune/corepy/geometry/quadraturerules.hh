// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_COREPY_GEOMETRY_QUADRATURERULES_HH
#define DUNE_COREPY_GEOMETRY_QUADRATURERULES_HH

#include <array>
#include <tuple>

#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/type.hh>

#include <dune/corepy/pybind11/pybind11.h>


namespace Dune
{

  namespace CorePy
  {

    namespace detail
    {

      // registerQuadraturePoint
      // -----------------------

      template< class QP >
      inline auto registerQuadraturePoint ( pybind11::handle scope, const char *name )
      {
        typedef typename QP::Vector Vector;
        typedef typename QP::Field Field;

        pybind11::class_< QP > cls( scope, name );

        cls.def( "__init__", [] ( QP &qp, const Vector &x, Field w ) { new (&qp) QP( x, w ); } );

        cls.def_property_readonly( "position", []( const QP &qp ) -> Vector { return qp.position(); } );
        cls.def_property_readonly( "weight", &QP::weight );

        return cls;
      }



      // registerQuadratureRule
      // ----------------------

      template< class Rule >
      inline auto registerQuadratureRule ( pybind11::handle scope, const char *name )
      {
        pybind11::class_< Rule > cls( scope, name );

        cls.def_property_readonly( "order", &Rule::order );
        cls.def_property_readonly( "type",  &Rule::type );

        cls.def( "__iter__", [] ( const Rule &rule ) { return pybind11::make_iterator( rule.begin(), rule.end() ); } );

        return cls;
      }

    } // namespace detail



    // registerQuadraturePoint
    // -----------------------

    template< class ctype, int dim >
    inline auto registerQuadraturePoint ( pybind11::handle scope, std::integral_constant< int, dim > )
    {
      typedef typename Dune::QuadraturePoint< ctype, dim > QP;

      static const std::string name = "QuadraturePoint" + std::to_string( dim );
      static const pybind11::class_< QP > cls = detail::registerQuadraturePoint< QP >( scope, name.c_str() );

      return cls;
    }

    template< class ctype, int ... dim >
    inline auto registerQuadraturePoint ( pybind11::handle scope, std::integer_sequence< int, dim ... > )
    {
      return std::make_tuple( registerQuadraturePoint< ctype >( scope, std::integral_constant< int, dim >() )... );
    }



    // registerQuadratureRule
    // ----------------------

    template< class ctype, int dim >
    inline auto registerQuadratureRule ( pybind11::handle scope, std::integral_constant< int, dim > )
    {
      registerQuadraturePoint< ctype, dim >( scope, std::integral_constant< int, dim >() );

      typedef typename Dune::QuadratureRule< ctype, dim > Rule;

      static const std::string name = "QuadratureRule" + std::to_string( dim );
      static const pybind11::class_< Rule > cls = detail::registerQuadratureRule< Rule >( scope, name.c_str() );

      return cls;
    }

    template< class ctype, int ... dim >
    inline auto registerQuadratureRule ( pybind11::handle scope, std::integer_sequence< int, dim ... > )
    {
      return std::make_tuple( registerQuadratureRule< ctype >( scope, std::integral_constant< int, dim >() )... );
    }



    // pyQuadratureRule
    // ----------------

    template< class ctype, int dim >
    inline static std::function< pybind11::object( GeometryType, int ) > pyQuadratureRule ( std::integral_constant< int, dim > )
    {
      return [] ( GeometryType type, int order ) -> pybind11::object {
          const int maxOrder = QuadratureRules< ctype, dim >::maxOrder( type );
          if( order > maxOrder )
            throw std::out_of_range( "Quadratures for this geometry type only implemented up to order " + std::to_string( maxOrder ) + "." );
          return pybind11::cast( QuadratureRules< ctype, dim >::rule( type, order ), pybind11::return_value_policy::reference );
        };
    }

    template< class ctype, int... dim >
    inline static auto pyQuadratureRule ( pybind11::handle scope, std::integer_sequence< int, dim... > )
    {
      registerQuadratureRule< ctype >( scope, std::integer_sequence< int, dim... >() );

      const std::array< std::function< pybind11::object( GeometryType, int ) >, sizeof...( dim ) > dispatch
        = {{ pyQuadratureRule< ctype >( std::integral_constant< int, dim >() )... }};

      return [ dispatch ] ( GeometryType type, int order ) {
          if( (type.dim() < 0) || (type.dim() >= sizeof...( dim )) )
            throw std::out_of_range( "dimension of geometry type must be in [" + std::to_string( 0 ) + ", " + std::to_string( sizeof...( dim )-1 ) + "]." );
          return dispatch[ type.dim() ]( type, order );
        };
    }

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_GEOMETRY_QUADRATURERULES_HH
