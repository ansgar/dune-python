#ifndef DUNE_COREPY_GEOMETRY_REFERENCEELEMENTS_HH
#define DUNE_COREPY_GEOMETRY_REFERENCEELEMENTS_HH

#include <array>
#include <functional>

#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>

#include <dune/corepy/pybind11/pybind11.h>

namespace Dune
{

  namespace CorePy
  {

    namespace detail
    {

      // registerReferenceElement
      // ------------------------

      template< class RefElement >
      inline auto registerReferenceElement ( pybind11::handle scope, const char *name )
      {
        pybind11::class_< RefElement > cls( scope, name );

        cls.def( "size", [] ( const RefElement &e, int c ) { return e.size( c ); } );
        cls.def( "size", [] ( const RefElement &e, int i, int c, int cc ) { return e.size( i, c, cc ); } );
        cls.def( "type", [] ( const RefElement &e, int i, int c ) { return e.type( i, c ); } );

        cls.def( "subEntity", &RefElement::subEntity );
        cls.def( "position", &RefElement::position );
        cls.def_property_readonly( "center", [] ( const RefElement &e ) { return e.position( 0, 0 ); } );
        cls.def( "volume", &RefElement::volume );
        cls.def( "integrationOuterNormal", &RefElement::integrationOuterNormal );

        cls.def_property_readonly( "type", [] ( const RefElement &e ) { return e.type(); } );

        return cls;
      }

    } // namespace detail



    // registerReferenceElement
    // ------------------------

    template< class ctype, int dim >
    inline auto registerReferenceElement ( pybind11::handle scope, std::integral_constant< int, dim > )
    {
      typedef typename Dune::ReferenceElement< ctype, dim > RefElement;

      static const std::string name = "ReferenceElement" + std::to_string( dim );
      static const pybind11::class_< RefElement > cls = detail::registerReferenceElement< RefElement >( scope, name.c_str() );

      return cls;
    }

    template< class ctype, int... dim >
    inline auto registerReferenceElement ( pybind11::handle scope, std::integer_sequence< int, dim ... > )
    {
      return std::make_tuple( registerReferenceElement< ctype >( scope, std::integral_constant< int, dim >() )... );
    }



    // pyReferenceElement
    // ------------------

    template< class ctype, int dim >
    inline static std::function< pybind11::object( GeometryType ) > pyReferenceElement ( std::integral_constant< int, dim > )
    {
      return [] ( GeometryType type ) -> pybind11::object {
          return pybind11::cast( ReferenceElements< ctype, dim >::general( type ), pybind11::return_value_policy::reference );
        };
    }

    template< class ctype, int... dim >
    inline static auto pyReferenceElement ( pybind11::handle scope, std::integer_sequence< int, dim... > )
    {
      registerReferenceElement< ctype >( scope, std::integer_sequence< int, dim... >() );

      const std::array< std::function< pybind11::object( GeometryType ) >, sizeof...( dim ) > dispatch
        = {{ pyReferenceElement< ctype >( std::integral_constant< int, dim >() )... }};

      return [ dispatch ] ( GeometryType type ) {
          if( (type.dim() < 0) || (type.dim() >= sizeof...( dim )) )
            throw std::out_of_range( "dimension of geometry type must be in [" + std::to_string( 0 ) + ", " + std::to_string( sizeof...( dim )-1 ) + "]." );
          return dispatch[ type.dim() ]( type );
        };
    }

  } // namespace FemPy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_GEOMETRY_REFERENCEELEMENTS_HH
