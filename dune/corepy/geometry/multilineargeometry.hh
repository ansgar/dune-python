// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_COREPY_GEOMETRY_MULTILINEARGEOMETRY_HH
#define DUNE_COREPY_GEOMETRY_MULTILINEARGEOMETRY_HH

#include <type_traits>

#include <dune/geometry/type.hh>
#include <dune/geometry/multilineargeometry.hh>

#include <dune/corepy/pybind11/pybind11.h>

namespace Dune
{
  namespace CorePy
  {

    namespace py = pybind11;

    // registerMemberFunctions
    // -----------------------

    template<typename MGeometry>
    struct RegisterMemberFunctions
    {
      RegisterMemberFunctions() {}
      void operator()(py::class_<MGeometry>& cls)
      {
        cls.def("localPosition"            , &MGeometry::local);
        cls.def("jacobianInverseTransposed", &MGeometry::jacobianInverseTransposed);
      }
    };



    // doNothing
    // ---------

    template<typename MGeometry>
    struct DoNothing
    {
      DoNothing() {}
      void operator()(py::class_<MGeometry>&) {}
    };



    // registerMultiLinearGeometryType
    // -------------------------------

    template<class ctype, int dim, int coordim>
    auto registerMultiLinearGeometryType(py::module scope)
    {
      typedef MultiLinearGeometry<ctype, dim, coordim> MGeometry;
      static std::unique_ptr<py::class_<MGeometry>> cls;

      static const std::string name = "MultiLinearGeometry";

      if (!cls)
      {
        cls = std::make_unique<py::class_<MGeometry>>(scope, name.c_str());

        cls->def("__init__",
            [](MGeometry& geom, Dune::GeometryType gt, py::list corners)
            {
              std::vector<FieldVector<double, 1>> cornerValues(corners.size());
              for (unsigned i = 0; i < corners.size(); ++i)
                cornerValues[i] = corners[i].cast<double>();

              new (&geom) MGeometry(gt, cornerValues);
            });

        // localPosition and jacobianInverseTransposed call
        // MatrixHelper::template (xT)rightInvA<m, n> where n has to be >= m (static assert)
        std::conditional_t<(coordim >= dim), RegisterMemberFunctions<MGeometry>, DoNothing<MGeometry> >{}(*cls);

        cls->def_property_readonly("affine" , [](const MGeometry& geom) { return geom.affine(); });
        cls->def_property_readonly("type"   , &MGeometry::type);
        cls->def_property_readonly("corners", &MGeometry::corners);
        cls->def_property_readonly("center" , &MGeometry::center);
        cls->def_property_readonly("volume" , &MGeometry::volume);
        cls->def("corner"                   , &MGeometry::corner);
        cls->def("integrationElement"       , &MGeometry::integrationElement);

        cls->def("jacobianTransposed",
            [](const MGeometry& geom, const typename MGeometry::LocalCoordinate& local) {
              return geom.jacobianTransposed(local);
            });

        cls->def("globalPosition",
            [](const MGeometry& geom, const typename MGeometry::LocalCoordinate& local) {
                return geom.global(local);
              });
      }
      else
      {
        scope.def(name.c_str(),
            [](Dune::GeometryType gt, py::list corners)
            {
              std::vector<FieldVector<double, 1>> cornerValues(corners.size());
              for (unsigned i = 0; i < corners.size(); ++i)
                cornerValues[i] = corners[i].cast<double>();
              return MGeometry(gt, cornerValues);
            });
      }

      return *cls;
    }

  } // namespace CorePy

} // namespace Dune

#endif // ifndef DUNE_COREPY_GEOMETRY_MULTILINEARGEOMETRY_HH
