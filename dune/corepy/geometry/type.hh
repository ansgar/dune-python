// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_COREPY_GEOMETRY_TYPE_HH
#define DUNE_COREPY_GEOMETRY_TYPE_HH

#include <dune/geometry/type.hh>

#include <dune/corepy/pybind11/pybind11.h>

namespace Dune
{
  namespace CorePy
  {

    namespace py = pybind11;

    py::class_<GeometryType> registerGeometryType(py::handle scope)
    {
      py::class_<GeometryType> cls(scope, "GeometryType");

      cls.def_property_readonly("isVertex", [](GeometryType &gt){return gt.isVertex();});
      cls.def_property_readonly("isLine", [](GeometryType &gt){return gt.isLine();});
      cls.def_property_readonly("isTriangle", [](GeometryType &gt){return gt.isTriangle();});
      cls.def_property_readonly("isQuadrilateral", [](GeometryType &gt){return gt.isQuadrilateral();});
      cls.def_property_readonly("isTetrahedron",[](GeometryType &gt){return gt.isTetrahedron();});
      cls.def_property_readonly("isPyramid",[](GeometryType &gt){return gt.isPyramid();});
      cls.def_property_readonly("isPrism", [](GeometryType &gt){return gt.isPrism();});
      cls.def_property_readonly("isHexahedron", [](GeometryType &gt){return gt.isHexahedron();});
      cls.def_property_readonly("isSimplex", [](GeometryType &gt){return gt.isSimplex();});
      cls.def_property_readonly("isCube", [](GeometryType &gt){return gt.isCube();});
      cls.def_property_readonly("isNone", [](GeometryType &gt){return gt.isNone();});

      cls.def_property_readonly("dim", &GeometryType::dim);

      return cls;
    }

  } // namespace CorePy

} // namespace Dune

#endif // ifndef DUNE_COREPY_GEOMETRY_TYPE_HH
