#ifndef DUNE_COREPY_FUNCTION_SIMPLEGRIDFUNCTION_HH
#define DUNE_COREPY_FUNCTION_SIMPLEGRIDFUNCTION_HH

#include <cassert>

#include <type_traits>
#include <utility>

#include <dune/corepy/pybind11/pybind11.h>

namespace Dune
{

  namespace CorePy
  {

    // SimpleLocalFunction
    // -------------------

    template< class GridView, class LocalEvaluator >
    class SimpleLocalFunction
    {
      typedef SimpleLocalFunction< GridView, LocalEvaluator > This;

    public:
      typedef typename GridView::template Codim< 0 >::Entity Element;

      typedef typename Element::Geometry::LocalCoordinate LocalCoordinate;
      typedef std::decay_t< std::result_of_t< LocalEvaluator( Element, LocalCoordinate ) > > Value;

      explicit SimpleLocalFunction ( LocalEvaluator localEvaluator ) : localEvaluator_( std::move( localEvaluator ) ) {}

      void bind ( const Element &element ) { element_ = &element; }
      void unbind () { element_ = nullptr; }

      Value operator() ( const LocalCoordinate &x ) const { return localEvaluator_( element(), x ); }

      const Element &element () const { assert( element_ ); return *element_; }

    private:
      const Element *element_ = nullptr;
      LocalEvaluator localEvaluator_;
    };



    // IsLocalEvaluator
    // ----------------

    template< class LE, class E, class X >
    std::true_type __isLocalEvaluator ( const LE &, const E &, const X &, decltype( std::declval< LE >()( std::declval< E >(), std::declval< X >() ) ) * = nullptr );

    std::false_type __isLocalEvaluator ( ... );

    template< class GP, class LE >
    struct IsLocalEvaluator
      : public decltype( Dune::CorePy::__isLocalEvaluator( std::declval< LE >(), std::declval< typename GP::template Codim< 0 >::Entity >(), std::declval< typename GP::template Codim< 0 >::Geometry::LocalCoordinate >() ) )
    {};



    // SimpleGridFunction
    // ------------------

    template< class GV, class LocalEvaluator >
    class SimpleGridFunction
    {
      typedef SimpleGridFunction< GV, LocalEvaluator > This;

    public:
      typedef GV GridView;

      typedef SimpleLocalFunction< GridView, LocalEvaluator > LocalFunction;

      typedef typename LocalFunction::Element Element;
      typedef typename LocalFunction::Value Value;

      SimpleGridFunction ( const GridView &gridView, LocalEvaluator localEvaluator )
        : gridView_( gridView ), localEvaluator_( std::move( localEvaluator ) )
      {}

      const GridView &gridView () const { return gridView_; }

      const LocalEvaluator &localEvaluator () const { return localEvaluator_; }

      friend LocalFunction localFunction ( const This &gf ) { return LocalFunction( gf.localEvaluator_ ); }

    protected:
      const GridView &gridView_;
      LocalEvaluator localEvaluator_;
    };



    // IsEvaluator
    // -----------

    template< class E, class X >
    std::true_type __isEvaluator ( const E &, const X &, decltype( std::declval< E >()( std::declval< X >() ) ) * = nullptr );

    std::false_type __isEvaluator ( ... );

    template< class GP, class E >
    struct IsEvaluator
      : public decltype( __isEvaluator( std::declval< E >(), std::declval< typename GP::template Codim< 0 >::Geometry::GlobalCoordinate >() ) )
    {};



    // LocalEvaluatorAdapter
    // ---------------------

    template< class Element, class Evaluator >
    struct LocalEvaluatorAdapter
    {
      typedef typename Element::Geometry::GlobalCoordinate GlobalCoordinate;
      typedef typename Element::Geometry::LocalCoordinate LocalCoordinate;

      typedef decltype( std::declval< Evaluator >()( std::declval< GlobalCoordinate >() ) ) Value;

      LocalEvaluatorAdapter ( Evaluator evaluator ) : evaluator_( std::move( evaluator ) ) {}

      Value operator () ( const GlobalCoordinate &x ) const { return evaluator_( x ); }
      Value operator () ( const Element &element, const LocalCoordinate &x ) const { return evaluator_( element.geometry().global( x ) ); }

    private:
      Evaluator evaluator_;
    };



    // SimpleGlobalGridFunction
    // ------------------------

    template< class GV, class Evaluator >
    class SimpleGlobalGridFunction
      : public SimpleGridFunction< GV, LocalEvaluatorAdapter< typename GV::template Codim< 0 >::Entity, Evaluator > >
    {
      typedef SimpleGlobalGridFunction< GV, Evaluator > This;
      typedef SimpleGridFunction< GV, LocalEvaluatorAdapter< typename GV::template Codim< 0 >::Entity, Evaluator > > Base;

    public:
      typedef typename Base::GridView GridView;
      typedef typename Base::Value Value;

      typedef typename GridView::template Codim< 0 >::Geometry::GlobalCoordinate GlobalCoordinate;

      SimpleGlobalGridFunction ( const GridView &gridView, Evaluator evaluator )
        : Base( gridView, std::move( evaluator ) )
      {}

      Value operator() ( const GlobalCoordinate &x ) const { return localEvaluator_( x ); }

    protected:
      using Base::localEvaluator_;
    };



    // simpleGridFunction
    // ------------------

    template< class GridView, class LocalEvaluator >
    inline static std::enable_if_t< IsLocalEvaluator< GridView, LocalEvaluator >::value, SimpleGridFunction< GridView, LocalEvaluator > >
    simpleGridFunction ( const GridView &gridView, LocalEvaluator localEvaluator )
    {
      return SimpleGridFunction< GridView, LocalEvaluator >( gridView, std::move( localEvaluator ) );
    }


    // simpleGridFunction
    // ------------------

    template< class GridView, class Evaluator >
    inline static std::enable_if_t< IsEvaluator< GridView, Evaluator >::value, SimpleGlobalGridFunction< GridView, Evaluator > >
    simpleGridFunction ( const GridView &gridView, Evaluator evaluator )
    {
      return SimpleGlobalGridFunction< GridView, Evaluator >( gridView, std::move( evaluator ) );
    }

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_FUNCTION_SIMPLEGRIDFUNCTION_HH
