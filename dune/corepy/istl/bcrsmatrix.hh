#ifndef DUNE_COREPY_ISTL_BCRSMATRIX_HH
#define DUNE_COREPY_ISTL_BCRSMATRIX_HH

#include <memory>
#include <stdexcept>
#include <string>
#include <tuple>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/corepy/pybind11/pybind11.h>
#include <dune/corepy/pybind11/stl.h>

#if HAVE_DUNE_ISTL
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrixmarket.hh>
#include <dune/istl/io.hh>
#endif // #if HAVE_DUNE_ISTL

namespace Dune
{

  namespace CorePy
  {

    namespace detail
    {

      template< class Matrix >
      struct CorrespondingVectors;

      template< class B, class A >
      struct CorrespondingVectors< BCRSMatrix< B, A > >
      {
        typedef BlockVector< typename CorrespondingVectors< B >::Domain, typename std::allocator_traits< A >::template rebind_alloc< typename CorrespondingVectors< B >::Domain > > Domain;
        typedef BlockVector< typename CorrespondingVectors< B >::Range, typename std::allocator_traits< A >::template rebind_alloc< typename CorrespondingVectors< B >::Range > > Range;
      };

      template< class K, int ROWS, int COLS >
      struct CorrespondingVectors< FieldMatrix< K, ROWS, COLS > >
      {
        typedef FieldVector< K, COLS > Domain;
        typedef FieldVector< K, ROWS > Range;
      };

    } // namespace detail



    // CorrespondingDomainVector
    // -------------------------

    template< class Matrix >
    using CorrespondingDomainVector = typename detail::CorrespondingVectors< Matrix >::Domain;




    // CorrespondingRangeVector
    // ------------------------

    template< class Matrix >
    using CorrespondingRangeVector = typename detail::CorrespondingVectors< Matrix >::Range;



    // registerBCRSMatrix
    // ------------------

#if HAVE_DUNE_ISTL
    template< class BCRSMatrix >
    pybind11::class_< BCRSMatrix > registerBCRSMatrix ( pybind11::handle scope, const char *clsName = "BCRSMatrix" )
    {
      typedef typename BCRSMatrix::field_type field_type;
      //typedef typename FieldTraits< field_type >::real_type real_type;
      typedef typename BCRSMatrix::size_type Size;
      typedef typename BCRSMatrix::BuildMode BuildMode;

      using pybind11::operator""_a;

      pybind11::class_< BCRSMatrix > cls( scope, clsName );

      pybind11::enum_< BuildMode > bm( cls, "BuildMode" );
      bm.value( "row_wise", BCRSMatrix::row_wise );
      bm.value( "random", BCRSMatrix::random );
      bm.value( "implicit", BCRSMatrix::implicit );
      // bm.value( "unknown", BCRSMatrix::unknown );
      bm.export_values();

      pybind11::enum_< typename BCRSMatrix::BuildStage > bs( cls, "BuildStage" );
      bs.value( "notAllocated", BCRSMatrix::notAllocated );
      bs.value( "building", BCRSMatrix::building );
      bs.value( "rowSizesBuilt", BCRSMatrix::rowSizesBuilt );
      bs.value( "built", BCRSMatrix::built );
      bs.export_values();

      // construction
      cls.def( "__init__", [] ( BCRSMatrix &self ) { new (&self) BCRSMatrix(); } );
      cls.def( "__init__", [] ( BCRSMatrix &self, Size rows, Size cols, Size nnz, BuildMode buildMode ) { new (&self) BCRSMatrix( rows, cols, nnz, buildMode ); }, "rows"_a, "cols"_a, "nnz"_a = 0, "buildMode"_a );
      cls.def( "__init__", [] ( BCRSMatrix &self, std::tuple< Size, Size > shape, Size nnz, BuildMode buildMode ) { new (&self) BCRSMatrix( std::get< 0 >( shape ), std::get< 1 >( shape ), nnz, buildMode ); }, "shape"_a, "nnz"_a = 0, "buildMode"_a );
      cls.def( "__init__", [] ( BCRSMatrix &self, Size rows, Size cols, Size average, double overflow, BuildMode buildMode ) { new (&self) BCRSMatrix( rows, cols, average, overflow, buildMode ); }, "rows"_a, "cols"_a, "average"_a, "overflow"_a, "buildMode"_a );
      cls.def( "__init__", [] ( BCRSMatrix &self, std::tuple< Size, Size > shape, Size average, double overflow, BuildMode buildMode ) { new (&self) BCRSMatrix( std::get< 0 >( shape ), std::get< 1 >( shape ), average, overflow, buildMode ); }, "shape"_a, "average"_a, "overflow"_a, "buildMode"_a );

      // shape
      cls.def_property_readonly( "rows", [] ( const BCRSMatrix &self ) { return self.N(); } );
      cls.def_property_readonly( "cols", [] ( const BCRSMatrix &self ) { return self.M(); } );
      cls.def_property_readonly( "shape", [] ( const BCRSMatrix &self ) { return std::make_tuple( self.N(), self.M() ); } );
      cls.def_property_readonly( "nonZeroes", [] ( const BCRSMatrix &self ) { return self.nonzeroes(); } );

      cls.def( "setSize", [] ( BCRSMatrix &self, Size rows, Size cols, Size nnz ) { self.setSize( rows, cols, nnz ); }, "rows"_a, "cols"_a, "nnz"_a = 0 );
      cls.def( "setSize", [] ( BCRSMatrix &self, std::tuple< Size, Size > shape, Size nnz ) { self.setSize( std::get< 0 >( shape ), std::get< 1 >( shape ), nnz ); }, "shape"_a, "nnz"_a = 0 );

      // build parameters
      cls.def_property( "buildMode", [] ( const BCRSMatrix &self ) {
          BuildMode bm = self.buildMode();
          if( bm == BCRSMatrix::unknown )
            return pybind11::object();
          else
            return pybind11::cast( bm );
        }, [] ( BCRSMatrix &self, BuildMode buildMode ) {
          self.setBuildMode( buildMode );
        } );
      cls.def_property_readonly( "buildStage", [] ( const BCRSMatrix &self ) { return self.buildStage(); } );
      cls.def( "setImplicitBuildModeParameters", [] ( BCRSMatrix &self, Size average, double overflow ) { self.setImplicitBuildModeParameters( average, overflow ); }, "average"_a, "overflow"_a );

      // random build mode
      cls.def( "setRowSize", [] ( BCRSMatrix &self, Size row, Size size ) { self.setrowsize( row, size ); }, "row"_a, "size"_a );
      cls.def( "getRowSize", [] ( const BCRSMatrix &self, Size row ) { return self.getrowsize( row ); }, "row"_a );
      cls.def( "endRowSizes", [] ( BCRSMatrix &self ) { self.endrowsizes(); } );

      cls.def( "addIndex", [] ( BCRSMatrix &self, Size row, Size col ) { self.addindex( row, col ); }, "row"_a, "col"_a );
      cls.def( "addIndex", [] ( BCRSMatrix &self, std::tuple< Size, Size > index ) { self.addindex( std::get< 0 >( index ), std::get< 1 >( index ) ); }, "index"_a );
      cls.def( "setIndices", [] ( BCRSMatrix &self, Size row, pybind11::buffer buffer ) {
          pybind11::buffer_info info = buffer.request();
          if( info.format != pybind11::format_descriptor< Size >::format() )
            throw std::invalid_argument( "Incompatible buffer format." );
          if( (info.ndim != 1) || (info.shape[ 0 ] != self.getrowsize( row )) )
            throw std::invalid_argument( "Indices must be a flat array with length" + std::to_string( self.getrowsize( row ) ) + "." );
          self.setIndices( row, static_cast< const Size * >( info.ptr ), static_cast< const Size * >( info.ptr ) + info.shape[ 0 ] );
        }, "row"_a, "indices"_a );
      cls.def( "setIndices", [] ( BCRSMatrix &self, Size row, std::vector< Size > indices ) {
          if( static_cast< Size >( indices.size() ) != self.getrowsize( row ) )
            throw std::invalid_argument( "len( indices ) must match previously set row size, i.e., " + std::to_string( self.getrowsize( row ) ) + "." );
          self.setIndices( row, indices.begin(), indices.end() );
        } );
      cls.def( "endIndices", [] ( BCRSMatrix &self ) { self.endindices(); } );

      // implicit build mode
      cls.def( "entry", [] ( BCRSMatrix &self, Size row, Size col ) { return self.entry( row, col ); }, "row"_a, "col"_a );
      cls.def( "entry", [] ( BCRSMatrix &self, std::tuple< Size, Size > index ) { return self.entry( std::get< 0 >( index ), std::get< 1 >( index ) ); }, "index"_a );
      cls.def( "compress", [] ( BCRSMatrix &self ) {
          auto result = self.compress();
          return std::make_tuple( result.maximum, result.overflow_total, result.avg, result.mem_ratio );
        } );

      // index access
      cls.def( "exists", [] ( const BCRSMatrix &self, Size row, Size col ) { return self.exists( row, col ); }, "row"_a, "col"_a );
      cls.def( "exists", [] ( const BCRSMatrix &self, std::tuple< Size, Size > index ) { return self.exists( std::get< 0 >( index ), std::get< 1 >( index ) ); } );

      // matrix-vector multiplication
      cls.def( "mv", [] ( const BCRSMatrix &self, const CorrespondingDomainVector< BCRSMatrix > &x, CorrespondingRangeVector< BCRSMatrix > &y ) {
          return self.mv( x, y );
        }, "x"_a, "y"_a );
      cls.def( "umv", [] ( const BCRSMatrix &self, const CorrespondingDomainVector< BCRSMatrix > &x, CorrespondingRangeVector< BCRSMatrix > &y ) {
          return self.umv( x, y );
        }, "x"_a, "y"_a );
      cls.def( "mmv", [] ( const BCRSMatrix &self, const CorrespondingDomainVector< BCRSMatrix > &x, CorrespondingRangeVector< BCRSMatrix > &y ) {
          return self.mmv( x, y );
        }, "x"_a, "y"_a );
      cls.def( "usmv", [] ( const BCRSMatrix &self, const field_type &alpha, const CorrespondingDomainVector< BCRSMatrix > &x, CorrespondingRangeVector< BCRSMatrix > &y ) {
          return self.usmv( alpha, x, y );
        }, "alpha"_a, "x"_a, "y"_a );

      cls.def( "mtv", [] ( const BCRSMatrix &self, const CorrespondingRangeVector< BCRSMatrix > &x, CorrespondingDomainVector< BCRSMatrix > &y ) {
          return self.mtv( x, y );
        }, "x"_a, "y"_a );
      cls.def( "umtv", [] ( const BCRSMatrix &self, const CorrespondingRangeVector< BCRSMatrix > &x, CorrespondingDomainVector< BCRSMatrix > &y ) {
          return self.umtv( x, y );
        }, "x"_a, "y"_a );
      cls.def( "mmtv", [] ( const BCRSMatrix &self, const CorrespondingRangeVector< BCRSMatrix > &x, CorrespondingDomainVector< BCRSMatrix > &y ) {
          return self.mmtv( x, y );
        }, "x"_a, "y"_a );
      cls.def( "usmtv", [] ( const BCRSMatrix &self, const field_type &alpha, const CorrespondingRangeVector< BCRSMatrix > &x, CorrespondingDomainVector< BCRSMatrix > &y ) {
          return self.usmtv( alpha, x, y );
        }, "alpha"_a, "x"_a, "y"_a );

      // norms
      cls.def_property_readonly( "frobenius_norm", [] ( const BCRSMatrix &self ) { return self.frobenius_norm(); } );
      cls.def_property_readonly( "frobenius_norm2", [] ( const BCRSMatrix &self ) { return self.frobenius_norm2(); } );
      cls.def_property_readonly( "infinity_norm", [] ( const BCRSMatrix &self ) { return self.infinity_norm(); } );
      cls.def_property_readonly( "infinity_norm_real", [] ( const BCRSMatrix &self ) { return self.infinity_norm_real(); } );

      // io
      cls.def( "load", [] ( BCRSMatrix &self, const std::string &fileName, std::string format ) {
          if( (format == "matrixmarket") || (format == "mm") )
            loadMatrixMarket( self, fileName );
          else
            throw std::invalid_argument( "Unknown format: "  + format );
        }, "fileName"_a, "format"_a );
      cls.def( "store", [] ( const BCRSMatrix &self, const std::string &fileName, std::string format ) {
          if( (format == "matrixmarket") || (format == "mm") )
            storeMatrixMarket( self, fileName );
          else if( format == "matlab" )
            writeMatrixToMatlab( self, fileName );
          else
            throw std::invalid_argument( "Unknown format: "  + format );
        }, "fileName"_a, "format"_a );

      return cls;
    }
#endif // #if HAVE_DUNE_ISTL

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_ISTL_BCRSMATRIX_HH
