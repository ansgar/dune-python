#ifndef DUNE_COREPY_ISTL_BVECTOR_HH
#define DUNE_COREPY_ISTL_BVECTOR_HH

#include <stdexcept>
#include <string>

#include <dune/corepy/pybind11/operators.h>
#include <dune/corepy/pybind11/pybind11.h>

#if HAVE_DUNE_ISTL
#include <dune/istl/bvector.hh>
#endif // #if HAVE_DUNE_ISTL

namespace Dune
{

  namespace CorePy
  {

    namespace detail
    {

      template< class K, int n >
      inline static void copy ( const char *ptr, const std::size_t *shape, const std::size_t *strides, Dune::FieldVector< K, n > &v )
      {
        if( *shape != static_cast< std::size_t >( n ) )
          throw std::runtime_error( "Invalid buffer size: " + std::to_string( *shape ) + " (should be: " + std::to_string( n ) + ")." );

        for( std::size_t i = 0; i < static_cast< std::size_t >( n ); ++i )
          v[ i ] = *reinterpret_cast< const K * >( ptr + i*(*strides) );
      }


#if HAVE_DUNE_ISTL
      template< class B, class A >
      inline static void copy ( const char *ptr, const std::size_t *shape, const std::size_t *strides, Dune::BlockVector< B, A > &v )
      {
        v.resize( *shape );
        for( std::size_t i = 0; i < *shape; ++i )
          copy( ptr + i*(*strides), shape+1, strides+1, v[ i ] );
      }
#endif // #if HAVE_DUNE_ISTL


      template< class BlockVector >
      inline static void copy ( pybind11::buffer buffer, BlockVector &v )
      {
        typedef typename BlockVector::field_type field_type;

        pybind11::buffer_info info = buffer.request();

        if( info.format != pybind11::format_descriptor< field_type >::format() )
          throw std::runtime_error( "Incompatible buffer format." );
        if( info.ndim != BlockVector::blocklevel )
          throw std::runtime_error( "Block vectors can only be initialized from one-dimensional buffers." );

        copy( static_cast< const char * >( info.ptr ), info.shape.data(), info.strides.data(), v );
      }

    } // namespace detail



    // registserBlockVector
    // --------------------

    template< class BlockVector >
    inline pybind11::class_< BlockVector > registerBlockVector ( pybind11::handle scope, const char *clsName = "BlockVector" )
    {
      typedef typename BlockVector::field_type field_type;
      typedef typename BlockVector::value_type value_type;
      typedef typename BlockVector::size_type size_type;

      using pybind11::operator""_a;

      pybind11::class_< BlockVector > cls( scope, clsName );

      cls.def( "__init__", [] ( BlockVector &self ) { new (&self) BlockVector(); } );
      cls.def( "__init__", [] ( BlockVector &self, size_type size ) { new (&self) BlockVector( size ); }, "size"_a );

      cls.def( "__init__", [] ( BlockVector &self, pybind11::buffer buffer ) {
          new (&self) BlockVector();
          detail::copy( buffer, self );
        } );

      cls.def( "assign", [] ( BlockVector &self, pybind11::buffer buffer ) { detail::copy( buffer, self ); }, "buffer"_a );
      cls.def( "assign", [] ( BlockVector &self, const BlockVector &x ) { self = x; }, "x"_a );

      cls.def( "copy", [] ( const BlockVector &self ) { return new BlockVector( self ); } );

      cls.def( "__getitem__", [] ( const BlockVector &self, size_type index ) -> value_type {
          auto pos = self.find( index );
          if( pos != self.end() )
            return *pos;
          else
            throw pybind11::index_error();
        }, "index"_a );

      cls.def( "__setitem__", [] ( BlockVector &self, size_type index, value_type value ) {
          auto pos = self.find( index );
          if( pos != self.end() )
            return *pos = value;
          else
            throw pybind11::index_error();
        }, "index"_a, "value"_a );

      cls.def( "__len__", [] ( const BlockVector &self ) { return self.N(); } );

      cls.def( pybind11::self += pybind11::self );
      cls.def( pybind11::self -= pybind11::self );
      cls.def( pybind11::self * pybind11::self );

      cls.def( pybind11::self *= field_type() );
      cls.def( pybind11::self /= field_type() );

      cls.def( "__add__", [] ( const BlockVector &self, const BlockVector &x ) { BlockVector *copy = new BlockVector( self ); *copy += x; return copy; }, "x"_a );
      cls.def( "__sub__", [] ( const BlockVector &self, const BlockVector &x ) { BlockVector *copy = new BlockVector( self ); *copy -= x; return copy; }, "x"_a );

      cls.def( "__div__", [] ( const BlockVector &self, field_type x ) { BlockVector *copy = new BlockVector( self ); *copy /= x; return copy; }, "x"_a );
      cls.def( "__mul__", [] ( const BlockVector &self, field_type x ) { BlockVector *copy = new BlockVector( self ); *copy *= x; return copy; }, "x"_a );
      cls.def( "__rmul__", [] ( const BlockVector &self, field_type x ) { BlockVector *copy = new BlockVector( self ); *copy *= x; return copy; }, "x"_a );

      cls.def_property_readonly( "one_norm", [] ( const BlockVector &self ) { return self.one_norm(); } );
      cls.def_property_readonly( "one_norm_real", [] ( const BlockVector &self ) { return self.one_norm_real(); } );
      cls.def_property_readonly( "two_norm", [] ( const BlockVector &self ) { return self.two_norm(); } );
      cls.def_property_readonly( "two_norm2", [] ( const BlockVector &self ) { return self.two_norm2(); } );
      cls.def_property_readonly( "infinity_norm", [] ( const BlockVector &self ) { return self.infinity_norm(); } );
      cls.def_property_readonly( "infinity_norm_real", [] ( const BlockVector &self ) { return self.infinity_norm_real(); } );

      cls.def_property_readonly( "capacity", [] ( const BlockVector &self ) { return self.capacity(); } );

      cls.def( "resize", [] ( BlockVector &self, size_type size ) { self.resize( size ); }, "size"_a );

      return cls;
    }

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_ISTL_BVECTOR_HH
