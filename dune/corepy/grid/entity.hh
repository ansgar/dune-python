// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_COREPY_GRID_ENTITY_HH
#define DUNE_COREPY_GRID_ENTITY_HH

#include <string>
#include <tuple>
#include <utility>

#include <dune/corepy/grid/geometry.hh>
#include <dune/corepy/pybind11/pybind11.h>
#include <dune/corepy/pybind11/operators.h>

namespace Dune
{

  namespace CorePy
  {

    // PyHierarchicIterator
    // --------------------

    template<class Entity>
    struct PyHierarchicIterator
    {
      typedef typename Entity::HierarchicIterator HierarchicIterator;

      PyHierarchicIterator(const Entity& e, int maxLevel)
        : it_(e.hbegin(maxLevel)), end_(e.hend(maxLevel))
      {}

      auto next()
      {
        if (it_ == end_)
          throw pybind11::stop_iteration();

        return *it_++;
      }

    private:
      HierarchicIterator it_;
      HierarchicIterator end_;
    };

    template <class Entity>
    struct CoordinateWrapper
    {
      typedef typename Entity::Geometry::LocalCoordinate LocalCoordinate;
      typedef typename Entity::Geometry::GlobalCoordinate GlobalCoordinate;
      CoordinateWrapper(const Entity &entity, const LocalCoordinate &xLocal)
        : entity_(entity), xLocal_(xLocal), xGlobal_(entity.geometry().global(xLocal)) {}
      const Entity &entity_;
      LocalCoordinate xLocal_;
      GlobalCoordinate xGlobal_;
    };


    // registerPyHierarchicIterator
    // ----------------------------

    template<class Entity>
    auto registerPyHierarchicIterator(pybind11::handle scope)
    {
      typedef PyHierarchicIterator<Entity> Iterator;

      pybind11::class_<Iterator>(scope, "PyHierarchicIterator")
        .def("__iter__", [] (Iterator& it) -> Iterator& { return it; })
        .def("__next__", &Iterator::next);
    }



    namespace detail
    {

      // registerExtendedEntityInterface
      // -------------------------------

      template< class Entity, std::enable_if_t< Entity::codimension == 0, int > = 0 >
      inline static void registerExtendedEntityInterface ( pybind11::class_< Entity > &cls )
      {
        cls.def_property_readonly( "father", [] ( const Entity &entity ) {
            return (entity.hasFather() ? pybind11::cast( entity.father() ) : pybind11::object( Py_None, true ));
          });

        cls.def_property_readonly( "geometryInFather", &Entity::geometryInFather );

        cls.def( "subEntities", &Entity::subEntities );
        cls.def( "isLeaf", &Entity::isLeaf );
        cls.def( "isRegular", &Entity::isRegular );
        cls.def( "isNew", &Entity::isNew );
        cls.def( "mightVanish", &Entity::mightVanish );
        cls.def( "hasBoundaryIntersections", &Entity::hasBoundaryIntersections );

        registerPyHierarchicIterator< Entity >( cls );
        cls.def( "descendants", [] ( const Entity &e, int maxLevel ) {
            return PyHierarchicIterator< Entity >( e, maxLevel );
          }, pybind11::keep_alive< 0, 1 >() );
      }

      template< class Cls >
      inline static void registerExtendedEntityInterface ( Cls &cls )
      {}



      // registerGridEntity
      // ------------------

      template< class Entity >
      inline static pybind11::class_< Entity > registerGridEntity ( pybind11::handle scope, const char *clsName )
      {
        pybind11::class_< Entity > cls( scope, clsName );

        cls.def_property_readonly( "codimension", [] ( const Entity &e) -> int { return e.codimension; } );
        cls.def_property_readonly( "dimension", [] ( const Entity &e ) -> int { return e.dimension; } );
        cls.def_property_readonly( "mydimension", [] ( const Entity &e ) -> int { return e.mydimension; } );

        cls.def_property_readonly( "geometry", &Entity::geometry );
        cls.def_property_readonly( "level", &Entity::level );
        cls.def_property_readonly( "type", &Entity::type );
        cls.def_property_readonly( "partitionType", &Entity::partitionType );

        cls.def( pybind11::self == pybind11::self );
        cls.def( pybind11::self != pybind11::self );

        auto coordinateCls = pybind11::class_< CoordinateWrapper<Entity> > ( scope, (std::string(clsName)+std::string("Coordinate")).c_str() );
        coordinateCls.def("__getitem__", [] (const CoordinateWrapper<Entity> &self, int i)
            -> double { return self.xGlobal_[i]; } );
        coordinateCls.def_property_readonly( "entity", [] (const CoordinateWrapper<Entity> &self)
            -> const Entity & { return self.entity_; } );
        coordinateCls.def_property_readonly( "xLocal", [] (const CoordinateWrapper<Entity> &self)
            -> const typename Entity::Geometry::LocalCoordinate & { return self.xLocal_; } );

        cls.def( "__call__", [] (const Entity &e, const typename Entity::Geometry::LocalCoordinate &xLocal )
            -> CoordinateWrapper<Entity> { return CoordinateWrapper<Entity>(e,xLocal); },
            pybind11::keep_alive< 0, 1 >() );

        registerExtendedEntityInterface( cls );

        return cls;
      }

    } // namespace detail



    // registerGridEntity
    // ------------------

    template< class Entity >
    pybind11::class_< Entity > registerGridEntity ( pybind11::handle scope )
    {
      registerGridGeometry< typename Entity::Geometry >( scope );

      static const std::string clsName = "Entity" + std::to_string( Entity::codimension );
      static const pybind11::class_< Entity > cls = detail::registerGridEntity< Entity >( scope, clsName.c_str() );
      return cls;
    }



    // registerGridEntities
    // --------------------

    template< class Grid, int... codim >
    inline static auto registerGridEntities ( pybind11::handle scope, std::integer_sequence< int, codim... > )
    {
      return std::make_tuple( registerGridEntity< typename Grid::template Codim< codim >::Entity >( scope )... );
    }

    template< class Grid >
    auto registerGridEntities ( pybind11::handle scope )
    {
      return registerGridEntities< Grid >( scope, std::make_integer_sequence< int, Grid::dimension+1 >() );
    }

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_GRID_ENTITY_HH
