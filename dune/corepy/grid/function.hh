// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_COREPY_GRID_FUNCTION_HH
#define DUNE_COREPY_GRID_FUNCTION_HH

#include <functional>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>

#include <dune/corepy/common/dimrange.hh>
#include <dune/corepy/function/simplegridfunction.hh>
#include <dune/corepy/grid/localview.hh>
#include <dune/corepy/grid/numpy.hh>
#include <dune/corepy/grid/object.hh>
#include <dune/corepy/grid/vtk.hh>

#include <dune/corepy/pybind11/pybind11.h>

namespace Dune
{

  namespace CorePy
  {

    // GridFunctionTraits
    // ------------------

    template< class GridFunction >
    struct GridFunctionTraits
      : public GridObjectTraits< GridFunction >
    {
      typedef typename GridObjectTraits< GridFunction >::LocalCoordinate LocalCoordinate;

      typedef std::decay_t< decltype( localFunction( std::declval< const GridFunction & >() ) ) > LocalFunction;
      typedef std::decay_t< decltype( std::declval< LocalFunction & >()( std::declval< const LocalCoordinate & >() ) ) > Range;
    };



    // registerGridFunction
    // --------------------

    template< class GridFunction, class... options >
    void registerGridFunction ( pybind11::handle scope, pybind11::class_< GridFunction, options... > cls )
    {
      using pybind11::operator""_a;

      typedef typename GridFunctionTraits< GridFunction >::Element Element;
      typedef typename GridFunctionTraits< GridFunction >::LocalCoordinate LocalCoordinate;
      typedef typename GridFunctionTraits< GridFunction >::LocalFunction LocalFunction;
      typedef typename GridFunctionTraits< GridFunction >::Range Range;

      pybind11::class_< LocalFunction > clsLocalFunction( cls, "LocalFunction" );
      registerLocalView< Element >( clsLocalFunction );
      clsLocalFunction.def( "__call__", [] ( const LocalFunction &self, const LocalCoordinate &x ) { return self( x ); }, "x"_a );
      clsLocalFunction.def_property_readonly( "dimRange", [] ( pybind11::object self ) { return pybind11::int_( DimRange< Range >::value ); } );

      cls.def_property_readonly( "grid", [] ( const GridFunction &self ) { return gridView( self ); } );
      cls.def_property_readonly( "dimRange", [] ( pybind11::object self ) { return pybind11::int_( DimRange< Range >::value ); } );

      cls.def( "localFunction", [] ( const GridFunction &self ) { return localFunction( self ); }, pybind11::keep_alive< 0, 1 >() );

      cls.def( "addToVTKWriter", &addToVTKWriter< GridFunction >, pybind11::keep_alive< 3, 1 >(), "name"_a, "writer"_a, "dataType"_a );

      cls.def( "cellData", [] ( const GridFunction &self, int level ) { return cellData( self, level ); }, "level"_a = 0 );
      cls.def( "pointData", [] ( const GridFunction &self, int level ) { return pointData( self, level ); }, "level"_a = 0 );
    }

    template< class GridFunction >
    pybind11::class_< GridFunction > registerGridFunction ( pybind11::handle scope, const char *clsName = "GridFunction" )
    {
      pybind11::class_< GridFunction > cls( scope, clsName );
      registerGridFunction( scope, cls );
      return cls;
    }



    namespace detail
    {

      // makePyGlobalGridFunction
      // ------------------------

      template< class GridView, int dimRange >
      auto makePyGlobalGridFunction ( const GridView &gridView, pybind11::function evaluate, std::integral_constant< int, dimRange > )
      {
        typedef typename GridView::template Codim< 0 >::Geometry::GlobalCoordinate Coordinate;
        return simpleGridFunction( gridView, [ evaluate ] ( const Coordinate &x ) {
            pybind11::gil_scoped_acquire acq;
            pybind11::object v( evaluate( x ) );
            return v.template cast< FieldVector< double, dimRange > >();
          } );
      }



      // registerPyGlobalGridFunction
      // ----------------------------

      template< class GridView, int dimRange >
      auto registerPyGlobalGridFunction ( pybind11::handle scope, const std::string &name, std::integral_constant< int, dimRange > )
      {
        typedef decltype( makePyGlobalGridFunction( std::declval< GridView >(), std::declval< pybind11::function >(), std::integral_constant< int, dimRange >() ) ) GridFunction;
        static const std::string clsName = name + std::to_string( dimRange );
        return CorePy::registerGridFunction< GridFunction >( scope, clsName.c_str() );
      };



      // pyGlobalGridFunction
      // --------------------

      template< class GridView, int dimRange >
      pybind11::object pyGlobalGridFunction ( const GridView &gridView, pybind11::function evaluate, pybind11::object parent )
      {
        return pybind11::cast( makePyGlobalGridFunction( gridView, std::move( evaluate ), std::integral_constant< int, dimRange >() ), pybind11::return_value_policy::move, parent );
      }

    } // namespace detail



    // defGlobalGridFunction
    // ---------------------

    template< class GridView, int... dimRange >
    auto defGlobalGridFunction ( pybind11::handle scope, std::string name, std::integer_sequence< int, dimRange... > )
    {
      std::ignore = std::make_tuple( detail::registerPyGlobalGridFunction< GridView >( scope, name, std::integral_constant< int, dimRange >() )... );

      typedef std::function< pybind11::object( const GridView &, pybind11::function, pybind11::object ) > Dispatch;
      std::array< Dispatch, sizeof...( dimRange ) > dispatch = {{ Dispatch( detail::pyGlobalGridFunction< GridView, dimRange > )... }};

      return [ dispatch ] ( pybind11::object gridView, pybind11::function evaluate ) {
          typename GridView::template Codim< 0 >::Geometry::GlobalCoordinate x( 0 );
          pybind11::gil_scoped_acquire acq;
          pybind11::object v( evaluate( x ) );
          const std::size_t dimR = len( v );
          if( dimR >= dispatch.size() )
            DUNE_THROW( NotImplemented, "globalGridFunction not implemented for dimRange = " + std::to_string( dimR ) );
          return dispatch[ dimR ]( gridView.cast< const GridView & >(), std::move( evaluate ), gridView );
        };
    }



    namespace detail
    {

      // makePyLocalGridFunction
      // -----------------------

      template< class GridView, int dimRange >
      auto makePyLocalGridFunction ( const GridView &gridView, pybind11::function evaluate, std::integral_constant< int, dimRange > )
      {
        typedef typename GridView::template Codim< 0 >::Entity Entity;
        typedef typename GridView::template Codim< 0 >::Geometry::LocalCoordinate Coordinate;
        return simpleGridFunction( gridView, [ evaluate ] ( const Entity &entity, const Coordinate &x ) {
            pybind11::gil_scoped_acquire acq;
            pybind11::object v( evaluate( entity, x ) );
            return v.template cast< FieldVector< double, dimRange > >();
          } );
      }


      // registerPyLocalGridFunction
      // ---------------------------

      template< class GridView, int dimRange >
      auto registerPyLocalGridFunction ( pybind11::handle scope, const std::string &name, std::integral_constant< int, dimRange > )
      {
        typedef decltype( makePyLocalGridFunction( std::declval< GridView >(), std::declval< pybind11::function >(), std::integral_constant< int, dimRange >() ) ) GridFunction;
        static const std::string clsName = name + std::to_string( dimRange );
        return CorePy::registerGridFunction< GridFunction >( scope, clsName.c_str() );
      };



      // pyGlobalGridFunction
      // --------------------

      template< class GridView, int dimRange >
      pybind11::object pyLocalGridFunction ( const GridView &gridView, pybind11::function evaluate, pybind11::object parent )
      {
        return pybind11::cast( makePyLocalGridFunction( gridView, std::move( evaluate ), std::integral_constant< int, dimRange >() ), pybind11::return_value_policy::move, parent );
      }

    } // namespace detail



    // defLocalGridFunction
    // --------------------

    template< class GridView, int... dimRange >
    auto defLocalGridFunction ( pybind11::handle scope, std::string name, std::integer_sequence< int, dimRange... > )
    {
      std::ignore = std::make_tuple( detail::registerPyLocalGridFunction< GridView >( scope, name, std::integral_constant< int, dimRange >() )... );

      typedef std::function< pybind11::object( const GridView &, pybind11::function, pybind11::object ) > Dispatch;
      std::array< Dispatch, sizeof...( dimRange ) > dispatch = {{ Dispatch( detail::pyLocalGridFunction< GridView, dimRange > )... }};

      return [ dispatch ] ( pybind11::object gp, pybind11::function evaluate ) {
          const GridView &gridView = gp.cast< const GridView & >();
          int dimR = -1;
          if( gridView.template begin< 0 >() != gridView.template end< 0 >() )
          {
            typename GridView::template Codim< 0 >::Geometry::LocalCoordinate x( 0 );
            pybind11::gil_scoped_acquire acq;
            pybind11::object v( evaluate( *gridView.template begin< 0 >(), x ) );
            dimR = len( v );
          }
          dimR = gridView.comm().max( dimR );
          if( dimR < 0 )
            DUNE_THROW( InvalidStateException, "Cannot create local grid function on empty grid" );
          if( static_cast< std::size_t >( dimR ) >= dispatch.size() )
            DUNE_THROW( NotImplemented, "localGridFunction not implemented for dimRange = " + std::to_string( dimR ) );
          return dispatch[ static_cast< std::size_t >( dimR ) ]( gridView, std::move( evaluate ), std::move( gp ) );
        };
    }

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_GRID_FUNCTION_HH
