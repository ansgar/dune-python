// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_COREPY_GRID_CAPABILITIES_HH
#define DUNE_COREPY_GRID_CAPABILITIES_HH

#include <type_traits>

#include <dune/grid/common/capabilities.hh>
#include <dune/grid/common/gridfactory.hh>

namespace Dune
{

  namespace CorePy
  {

    namespace Capabilities
    {

      using namespace Dune::Capabilities;

      template< class Grid >
      struct HasGridFactory
        : public std::integral_constant< bool, !isCartesian< Grid >::v >
      {};

      template< class Grid, int codim >
      struct canIterate
        : public std::integral_constant< bool, hasEntity< Grid, codim >::v >
      {};

    } // namespace Capabilities

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_GRID_CAPABILITIES_HH
