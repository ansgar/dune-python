// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_COREPY_GRID_INTERSECTION_HH
#define DUNE_COREPY_GRID_INTERSECTION_HH

#include <dune/corepy/grid/geometry.hh>
#include <dune/corepy/pybind11/pybind11.h>
#include <dune/corepy/pybind11/operators.h>
#include <dune/corepy/pybind11/extensions.h>

namespace Dune
{

  namespace CorePy
  {

    namespace detail
    {

      // registerGridIntersection
      // ------------------------

      template< class Intersection >
      pybind11::class_< Intersection > registerGridIntersection ( pybind11::handle scope, const char *clsName )
      {
        pybind11::class_< Intersection > cls( scope, clsName );

        // information on boundary intersections
        cls.def_property_readonly( "boundary", &Intersection::boundary );
        cls.def_property_readonly( "boundarySegmentIndex", [] ( const Intersection &i ) {
            return (i.boundary() ? pybind11::cast( i.boundarySegmentIndex() ) : pybind11::object( Py_None, true ));
          } );

        // conformity
        cls.def_property_readonly( "conforming", &Intersection::conforming );

        // geometric information
        cls.def_property_readonly( "geometry", &Intersection::geometry );
        cls.def_property_readonly( "type", &Intersection::type );

        // information on inside entity
        cls.def_property_readonly( "inside", &Intersection::inside );
        cls.def_property_readonly( "geometryInInside", &Intersection::geometryInInside );
        cls.def_property_readonly( "indexInInside", &Intersection::indexInInside );

        // information on ouside entity
        cls.def_property_readonly( "outside", [] ( const Intersection &i ) {
              return (i.neighbor() ? pybind11::cast( i.outside() ) : pybind11::object( Py_None, true ));
            } );
        cls.def_property_readonly( "geometryInOutside", [] ( const Intersection &i ) {
              return (i.neighbor() ? pybind11::cast( i.geometryInOutside() ) : pybind11::object( Py_None, true ));
            } );
        cls.def_property_readonly( "indexInOutside", [] ( const Intersection &i ) {
              return (i.neighbor() ? pybind11::cast( i.indexInOutside() ) : pybind11::object( Py_None, true ));
            } );

        // outer normals
        cls.def_property_readonly( "centerUnitOuterNormal", &Intersection::centerUnitOuterNormal);
        cls.def( "outerNormal", &Intersection::outerNormal );
        cls.def( "integrationOuterNormal", &Intersection::integrationOuterNormal );
        cls.def( "unitOuterNormal", &Intersection::unitOuterNormal );

        // comparison
        cls.def( pybind11::self == pybind11::self );
        cls.def( pybind11::self != pybind11::self );

        return cls;
      }

    } // namespace detail



    // registerGridIntersection
    // ------------------------

    template< class Intersection >
    pybind11::class_< Intersection > registerGridIntersection ( pybind11::handle scope )
    {
      registerGridGeometry< typename Intersection::Geometry >( scope );

      static const pybind11::class_< Intersection > cls = detail::registerGridIntersection< Intersection >( scope, "Intersection" );
      return cls;
    }

  } // namespace CorePy

} // namespace Dune

#endif //#ifndef DUNE_COREPY_GRID_INTERSECTION_HH
