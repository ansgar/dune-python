// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_COREPY_GRID_MAPPER_HH
#define DUNE_COREPY_GRID_MAPPER_HH

#include <functional>

#include <dune/grid/common/mcmgmapper.hh>

#include <dune/corepy/common/typeregistry.hh>
#include <dune/corepy/pybind11/pybind11.h>
#include <dune/corepy/pybind11/functional.h>

namespace Dune
{
  namespace CorePy
  {

    namespace py = pybind11;

    // registerMemberFunctions
    // -----------------------

    template<class GridView, class Mapper, int codim>
    void registerMemberFunctions_(py::class_<Mapper>& cls)
    {
      cls.def("index", &Mapper::template index<typename GridView::template Codim<codim>::Entity>);

      cls.def("contains",
        [] (const Mapper& mapper, const typename GridView::template Codim<codim>::Entity& e) {
          typename Mapper::Index index;
          bool res = mapper.contains(e, index);

          return py::make_tuple(res, index);
        });
    }

    template<class GridView, class Mapper, int... codim>
    void registerMemberFunctions(py::class_<Mapper>& cls, std::integer_sequence<int, codim...>)
    {
      std::ignore = std::make_tuple((registerMemberFunctions_<GridView, Mapper, codim>(cls), 0)...);
    }



    // registerMapper
    // --------------

    template<class Mapper, class GridView>
    py::class_<Mapper> registerMapper(py::handle scope, const char *name)
    {
      py::class_<Mapper> cls(scope, name);

      cls.def("__len__", &Mapper::size);
      cls.def("subIndex", &Mapper::subIndex);
      cls.def("contains",
          [] (Mapper& instance, const typename GridView::template Codim<0>::Entity& e, int i, int cc) {
            typename Mapper::Index index;
            bool res = instance.contains(e, i, cc, index);

            return py::make_tuple(res, index);
          });

      registerMemberFunctions<GridView, Mapper>(
          cls, std::make_integer_sequence<int, GridView::dimension+1>());

      return cls;
    }



    // Helper class template Layout (cf. doc)
    // --------------------------------------

    template<int dimgrid>
    class Layout
    {
    public:

      Layout(py::function f) :
        contains_(f) {}

      bool contains(Dune::GeometryType gt)
      {
        py::bool_ res = contains_(gt);
        return res.cast<bool>();
      }

    private:
      py::function contains_;
    };



    // registerMultipleCodimMultipleGeomTypeMapper
    // -------------------------------------------

    template<typename GridView>
    auto registerMultipleCodimMultipleGeomTypeMapper(py::handle scope)
    {
      typedef MultipleCodimMultipleGeomTypeMapper<GridView, Layout> MCMGMapper;
      auto entry = Dune::CorePy::typeRegistry().insert<MCMGMapper>("Dune::MultipleCodimMultipleGeomTypeMapper",
          {"dune/grid/common/mcmgmapper.hh","dune/corepy/grid/mapper.hh"},
          Dune::CorePy::typeRegistry().at<GridView>(), "Dune::CorePy::Layout");
      auto cls = registerMapper<MCMGMapper, GridView>(scope, "MultipleCodimMultipleGeomTypeMapper");
      Dune::CorePy::typeRegistry().exportToPython(cls,entry.first->second);
      cls.def("__init__",
          [] (MCMGMapper& instance, const GridView& gridView, py::function contains) {
            Layout<GridView::dimension> layout(contains);
            new (&instance) MCMGMapper(gridView, layout);
          },
          py::keep_alive<1, 2>());

      return cls;
    }

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_GRID_MAPPER_HH
