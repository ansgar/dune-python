// -*- tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_COREPY_GRID_GEOMETRY_HH
#define DUNE_COREPY_GRID_GEOMETRY_HH

#include <string>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/corepy/pybind11/pybind11.h>

namespace Dune
{

  namespace CorePy
  {

    // PyCornerRange
    // -------------

    template<class Geometry>
    struct PyCorners
    {
      PyCorners(const Geometry &geometry, pybind11::object ref)
        : geometry_(geometry), ref_(ref)
      {}

      const Geometry& geometry() { return geometry_; }

    private:
      const Geometry &geometry_;
      pybind11::object ref_;
    };


    // PyCornerIterator
    // ----------------

    template<class Geometry>
    struct PyCornerIterator
    {
      PyCornerIterator(const PyCorners<Geometry> corners) : corners_(corners) {}

      typename Geometry::GlobalCoordinate next()
      {
        if(index_ == corners_.geometry().corners())
          throw pybind11::stop_iteration();

        return corners_.geometry().corner(index_++);
      }

    private:
      PyCorners<Geometry> corners_;
      int index_ = 0;
    };



    namespace detail
    {

      // registerGridGeometry
      // --------------------

      template< class Geometry >
      pybind11::class_< Geometry > registerGridGeometry ( pybind11::handle scope, const char *clsName )
      {
        const int mydimension = Geometry::mydimension;
        const int coorddimension = Geometry::coorddimension;

        typedef typename Geometry::ctype ctype;
        typedef FieldVector< ctype, mydimension > LocalCoordinate;
        //typedef FieldVector< ctype, coorddimension > GlobalCoordinate;

        pybind11::class_< Geometry > cls( scope, clsName );

        pybind11::class_< PyCornerIterator< Geometry > > itCls( cls, "CornerIterator" );
        itCls.def( "__iter__", [] ( PyCornerIterator< Geometry > &it ) -> PyCornerIterator< Geometry > & { return it; } );
        itCls.def( "__next__", &PyCornerIterator< Geometry >::next );

        pybind11::class_< PyCorners< Geometry > > cCls( cls, "Corners" );
        cCls.def( "__iter__", [] ( const PyCorners< Geometry > &c ) { return PyCornerIterator< Geometry >( c ); } );

        cls.def_property_readonly( "corners", [] ( pybind11::object geo ) {
            return PyCorners< Geometry >( geo.cast< const Geometry & >(), geo );
          } );

        cls.def_property_readonly( "center", &Geometry::center );
        cls.def_property_readonly( "volume", &Geometry::volume );

        cls.def_property_readonly( "affine", &Geometry::affine );

        cls.def( "position", &Geometry::global );
        cls.def( "integrationElement", &Geometry::integrationElement );
        cls.def( "jacobianTransposed", [] ( const Geometry &geometry, const LocalCoordinate &x ) {
            return static_cast< FieldMatrix< ctype, mydimension, coorddimension > >( geometry.jacobianTransposed( x ) );
          } );
        cls.def( "jacobianInverseTransposed", [] ( const Geometry &geometry, const LocalCoordinate &x ) {
            return static_cast< FieldMatrix< ctype, coorddimension, mydimension > >( geometry.jacobianInverseTransposed( x ) );
          } );

        cls.def_property_readonly( "domain", [] ( const Geometry &geo ) -> const Dune::ReferenceElement< ctype, mydimension > & {
            return Dune::ReferenceElements< ctype, mydimension >::general( geo.type() );
          }, pybind11::return_value_policy::reference );

        return cls;
      }

    } // namespace detail



    // registerGridGeometry
    // --------------------

    template< class Geometry >
    pybind11::class_< Geometry > registerGridGeometry ( pybind11::handle scope )
    {
      static const std::string clsName = "Geometry" + std::to_string( Geometry::mydimension );
      static const pybind11::class_< Geometry > cls = detail::registerGridGeometry< Geometry >( scope, clsName.c_str() );
      return cls;
    }

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_GRID_GEOMETRY_HH
