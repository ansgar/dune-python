#ifndef DUNE_COREPY_FUNCTIONS_SPACE_HH
#define DUNE_COREPY_FUNCTIONS_SPACE_HH

#include <cstddef>

#include <tuple>
#include <type_traits>
#include <utility>

#include <dune/functions/functionspacebases/defaultglobalbasis.hh>

#include <dune/corepy/common/dimrange.hh>
#include <dune/corepy/common/fmatrix.hh>
#include <dune/corepy/common/fvector.hh>
#include <dune/corepy/functions/discretefunction.hh>

#include <dune/corepy/pybind11/complex.h>
#include <dune/corepy/pybind11/pybind11.h>


namespace Dune
{

  namespace CorePy
  {

    // MultiIndex
    // ----------

    template< class FactoryTag >
    using MultiIndex = Dune::ReservedVector< std::size_t, FactoryTag::requiredMultiIndexSize >;



    // NodeFactory
    // -----------

    template< class GridView, class FactoryTag >
    using NodeFactory = std::decay_t< decltype( std::declval<FactoryTag>().template build< MultiIndex< FactoryTag > >( std::declval<GridView>() ) ) >;



    // DefaultGlobalBasis
    // ------------------

    template< class GridView, class FactoryTag >
    struct DefaultGlobalBasis
      : public Dune::Functions::DefaultGlobalBasis< NodeFactory< GridView, FactoryTag > >
    {
      typedef Dune::Functions::DefaultGlobalBasis< NodeFactory< GridView, FactoryTag > > Base;

      explicit DefaultGlobalBasis ( const GridView &gridView )
        : Base( FactoryTag().template build< MultiIndex< FactoryTag > >( gridView ) )
      {}
    };



    // registerGlobalBasis
    // -------------------

    template <class Basis>
    struct LocalViewWrapper : public Basis::LocalView
    {
      typedef typename Basis::LocalView Base;
      typedef typename Base::Element EntityType;
      LocalViewWrapper(Basis &b) : Base(b) {}
      void bind ( pybind11::object &obj )
      {
        obj_ = obj;
        const EntityType &entity = obj.template cast<const EntityType&>();
        Base::bind(entity);
      }
      void unbind ( )
      {
        obj_.release();
        Base::unbind();
      }
      pybind11::object obj_;
    };
    template <class Basis>
    struct LocalIndexWrapper
    {
      typedef typename Basis::LocalView View;
      LocalIndexWrapper(Basis &basis) : indexSet_(basis.localIndexSet()) {}
      std::vector<int> index(int index)
      {
        auto ind = indexSet_.index(index);
        std::vector<int> ret(ind.size());
        for (int i=0;i<ind.size();++i) ret[i] = ind[i];
        return ret;
      }

      void bind ( pybind11::object &obj )
      {
        obj_ = obj;
        const LocalViewWrapper<Basis> &view = obj.template cast<const LocalViewWrapper<Basis>>();
        indexSet_.bind(view);
      }
      void unbind ( )
      {
        obj_.release();
        indexSet_.unbind();
      }
      pybind11::object obj_;
      typename Basis::LocalIndexSet indexSet_;
    };

    template< class GlobalBasis, class... options >
    void registerGlobalBasis ( pybind11::module module, pybind11::class_< GlobalBasis, options... > &cls )
    {
      using pybind11::operator""_a;

      typedef Dune::TypeTree::HybridTreePath<> DefaultTreePath;

      const std::size_t dimRange = DimRange< typename GlobalBasis::NodeFactory::template Node< DefaultTreePath > >::value;

      cls.def( "__init__", [] ( GlobalBasis &self, const typename GlobalBasis::GridView &gridView ) {
            new (&self) GlobalBasis( gridView );
          }, py::keep_alive< 1, 2 >() );

      cls.def_property_readonly( "dimRange", [] ( pybind11::handle self ) { return pybind11::int_( dimRange ); } );

      typedef LocalViewWrapper< GlobalBasis > LocalView;
      auto lventry = typeRegistry().insert< LocalView >( "Dune::CorePy::LocalViewWrapper", { "dune/corepy/functions/globalbasis.hh" }, typeRegistry().at< GlobalBasis >() );
      auto lv = py::class_< LocalView >( module, "LocalView" );
      Dune::CorePy::typeRegistry().exportToPython( lv, lventry.first->second );
      lv.def( "bind", &LocalView::bind );
      lv.def( "unbind", &LocalView::unbind );
      lv.def( "__len__", [] ( LocalView &self ) -> int { return self.size(); } );

      typedef LocalIndexWrapper< GlobalBasis > LocalIndex;
      auto isentry = typeRegistry().insert< LocalIndex >( "Dune::CorePy::LocalIndexWrapper", { "dune/corepy/functions/globalbasis.hh" }, typeRegistry().at< GlobalBasis >() );
      auto lis = py::class_<LocalIndex>( module, "LocalIndex" );
      Dune::CorePy::typeRegistry().exportToPython( lis, isentry.first->second );
      lis.def( "bind", &LocalIndex::bind );
      lis.def( "unbind", &LocalIndex::unbind );
      lis.def( "index", [] ( LocalIndex &localIndexSet, int index ) { return localIndexSet.index( index ); });

      cls.def( "localView", [] ( GlobalBasis &self ) -> LocalView { return LocalView( self ); }, py::keep_alive< 0, 1 >() );
      cls.def( "localIndexSet", [] ( GlobalBasis &self ) -> LocalIndex { return LocalIndex( self ); }, py::keep_alive< 0, 1 >() );
      cls.def_property_readonly( "dimension", [] ( GlobalBasis &self ) -> int { return self.dimension(); } );

      typedef Dune::FieldVector< double, dimRange > Range;
      typedef Dune::Functions::DiscreteGlobalBasisFunction< GlobalBasis, DefaultTreePath, HierarchicPythonVector< double >, DefaultNodeToRangeMap< GlobalBasis, DefaultTreePath >, Range > DiscreteFunction;
      pybind11::class_< DiscreteFunction > clsDiscreteFunction( cls, "DiscreteFunction" );
      registerDiscreteFunction( cls, clsDiscreteFunction );

      cls.def("asFunction", [] ( GlobalBasis &self, pybind11::buffer dofVector ) {
          auto nodeToRangeMapPtr = std::make_shared< DefaultNodeToRangeMap< GlobalBasis, DefaultTreePath > >( makeDefaultNodeToRangeMap( self, DefaultTreePath() ) );
          auto basisPtr = Dune::wrap_or_move( self );
          auto vectorPtr = std::make_shared< HierarchicPythonVector< double > >( dofVector );
          return new DiscreteFunction( basisPtr, DefaultTreePath(), vectorPtr, nodeToRangeMapPtr );
        }, pybind11::keep_alive< 0, 1 >(), pybind11::keep_alive< 0, 2 >(), "dofVector"_a );
    }

  } // namespace CorePy

} // namespace Dune

#endif // #ifndef DUNE_COREPY_FUNCTIONS_SPACE_HH
