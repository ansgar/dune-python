#!/bin/bash

TESTDIR=$(realpath -e $(dirname $0))
WORKDIR=${WORKDIR:-/tmp}

mkdir $WORKDIR/source
mkdir $WORKDIR/build
mkdir $WORKDIR/install

cd $WORKDIR/source
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git
git clone https://gitlab.dune-project.org/extensions/dune-spgrid.git

CMAKE_FLAGS=" \
  -DBUILD_SHARED_LIBS=TRUE \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=\"${WORKDIR}/install\" \
  -DPYTHON_INSTALL_LOCATION=user \
  -DDUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS=TRUE \
  -DENABLE_HEADERCHECK=FALSE \
  -DCMAKE_DISABLE_FIND_PACKAGE_LATEX=TRUE \
  -DCMAKE_DISABLE_DOCUMENTATION=TRUE \
  -DDUNE_PYTHON_FORCE_PYTHON2=TRUE \
"
export CMAKE_FLAGS
export DUNE_CONTROL_PATH=${TESTDIR}:.
./dune-common/bin/dunecontrol --builddir=$WORKDIR/build all
./dune-common/bin/dunecontrol --builddir=$WORKDIR/build make install

export DUNE_CMAKE_FLAGS=${CMAKE_FLAGS}
export DUNE_CONTROL_PATH=$WORKDIR/install
export PKG_CONFIG_PATH=$WORKDIR/install/lib/pkgconfig:$PKG_CONFIG_PATH

cd ${TESTDIR}/demo
python test.py
