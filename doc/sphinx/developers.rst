.. contents::
   :local:
   :backlinks: top



.. _introduction_developers:

***********************************
Introduction
***********************************

This document aims to provide the technical details behind `dune_corepy`. If you do not know this module, perhaps you could read first :doc:`index` and then :doc:`usage` to get a first insight.

`dune_corepy` is using `pybind11`_ to export the DUNE interface to Python. Be sure to have read at least the `Object-oriented code section`_ before reading this document and do not hesitate to give a look at the `Advanced topics section`_ when you do not understand some details of the examples given in this page. 

.. _pybind11 : http://pybind11.readthedocs.io/en/latest/
.. _Object-oriented code section : http://pybind11.readthedocs.io/en/latest/classes.html
.. _Advanced topics section : http://pybind11.readthedocs.io/en/latest/advanced.html



.. _overview_developers:

***********************************
Overview
***********************************

`dune-corepy` aims to provide Python bindings for the DUNE grid interface. Actually, this grid interface is compound by a set of interfaces, as described in the `documentation`_. Thus, `dune-corepy` implements bindings for each of these interfaces.

Before we go any further, it is important to talk about the different C++ types you may encounter in the DUNE codebase, in order to understand more clearly the sources of `dune-corepy`. In fact, you have to distinguish three different C++ types:

* types that you can export directly using `pybind11` and its `pybind11::class_`, as explained in the
  `Object-oriented code section`_ of the pybind11 documentation. We refer to those types as
  **regular**.

  For example, DUNE defines the `GeometryType` class, useful when manipulating entities of a grid. As
  a **regular** type, this `GeometryType` is directly exported to Python like this:

  .. code-block:: c++

      void registerGeometryType(pybind11::handle scope)
      {
        pybind11::class_<Dune::GeometryType> cls(scope, "GeometryType");

        cls.def("isVertex", &GeometryType::isVertex);
        cls.def(...);
        ...
      }

  Where `scope` is actually the Python module from which `GeometryType` will be reachable.

* internal DUNE classes that need to be wrapped before they can be exported to Python. These classes
  cannot be initialized directly from Python because they have no constructor. Thus, the (C++)
  wrapper class stores a reference to an instance of the actual DUNE class and forwards all the method
  calls to it.

  For an example, see :ref:`wrappers_developers`. A skeleton for this kind of bindings could be something like
  that:

  .. code-block:: c++

      struct FooTypeWrapper
      {
        using FooType = Dune::FooType;

        FooTypeWrapper() {} // The constructor is essential to export the wrapper with pybind11

        /*
         * Forward here all the method calls to the
         * actual methods of the Dune::FooType class
         *
         * You may have to store a reference to an instance
         * of the actual DUNE class to do that or just call
         * FooType::someMethod(someArgs) if someMethod is static
         *
         \*/
      }

      void registerFooType(pybind11::handle scope)
      {
        pybind11::class_<FooTypeWrapper> cls(scope, "FooType"); // Now, you register your wrapper within pybind11 to export it to Python

        // And finally, you expose the interface of FooType, where the methods
        // of the wrapper will redirect to the original ones:

        cls.def("someMethod", &FooTypeWrapper::someMethod);
        cls.def(...);
        ...
      }

* finally, we need to distinguish classes that are template classes, parameterized by an
  `Implementation` class. For example:

  .. code-block:: c++

      template<class Impl>
      class Grid
      {
        ...
      }

  Actually, this kind of class defines an interface, a concept. Users want to use it without any
  regard to the implementation behind.

  For example, in the above code, `Grid` is an interface with its implementation `Impl` as a template
  parameter. A user could then write some algorithm using this `Grid` interface and test it with
  different grid implementations like `YaspGrid`, `ALUGrid`, `UGGrid`, etc. just by instantiating this
  `Grid` template class at compile-time, writing something like `Grid<Yaspgrid>`, `Grid<ALUGrid>`, etc.

  In fact, Dune implements its grid interface like that: a set of template classes that depend on a
  specific implementation, plugged into the grid interface at compile-time.
  Exporting this mechanism to Python is complicated and this is why we are dedicating the entire
  following section to discuss this.

.. _documentation : https://beta.dune-project.org/doxygen/master/group__GridInterface.html
.. _Object-oriented code section : http://pybind11.readthedocs.io/en/latest/classes.html



.. _database_developers:

***********************************
Database approach
***********************************

Actually, exporting a class of this kind, which is an interface, means instantiating it with the implementation of your choice. Thus, the idea is then to be generic as possible to delay that instantiation to the Python runtime so the user can choose the implementation he wants without much troubles. 

To achieve that, `dune-corepy` implements a database mechanism: the general idea is to use a database with one entry for each implementation of your interface, defining the typedefs and includes needed to instantiate the interface.
Then, a Python module called `Generator` is used at runtime to retrieve the relevant entry of the database, depending on the parameters passed by the user and which describe the implementation of the interface he wants (e.g. basically a string). At this point, the `Generator` invokes CMake to compile your register functions with the type of implementation you chose and thus, you finally end up with a Python binding of your interface class (that powers up the implementation you just chose).

Let's see a detailed example to illustrate these explanations:

We want to export the `grid abstract base class`_ that provides the main interface for all grid implementations, so we make the register function a template function as following:

.. code-block:: c++

  template<typename Grid>
  void registerGrid(pybind11::handle scope)
  {
    pybind11::class_<Grid>(scope, "Grid") cls;

    cls.def("__repr__",     ...);
    cls.def("globalRefine", ...);
    cls.def("loadBalance",  ...);
    ...
  }
 
To provide the information required to export an implementation of this grid interface, we use Python dictionaries containing information to generate the typedefs for the implementation and the required include statements needed to compile the register function. For example:

.. code-block:: python

  "YaspGrid" :
  {
    "type"      : "Dune::YaspGrid< $(dimgrid), $(coordinates)< $(ctype), $(dimgrid) > >",
    "default"   : [ "ctype=double", "coordinates=equidistant" ],
    "translate" : { "coordinates" : [ "equidistant=Dune::EquidistantCoordinates", "tensorproduct=Dune::TensorProductCoordinates" ] },
    "include"   : [ "dune/grid/yaspgrid.hh", "dune/grid/io/file/dgfparser/dgfyasp.hh" ]
  }

The first line provides the information on how to construct the correct typedef for the Dune class:

.. code-block:: c++

  template<int dim, class Coordinates = EquidistantCoordinates<double, dim> >
  class YaspGrid : public GridDefaultImplementation<dim, dim, typename Coordinates::ctype, YaspGridFamily<dim, Coordinates> >;

The last line contains information about which include statements are required to compile the register function, i.e., 

.. code-block:: c++

  #include <dune/grid/yaspgrid.hh> 
  #include <dune/grid/io/file/dgfparser/dgfyasp.hh>

The remaining two lines provide default values for some other grid parameters.

On the fly code generation is done with the python module `dune.generator`. 

.. autoclass:: dune.generator.generator.Generator
   :members:
   :special-members: __init__

In the above example the extension module for our grid implementation would be generated by calling:

.. code-block:: python

  yaspModule = Generator("grid").getModule("YaspGrid", dimgrid=2, dimworld=2)

And the resulting header would be as following: 

.. code-block:: c++

  #include <dune/grid/yaspgrid.hh> 
  #include <dune/grid/io/file/dgfparser/dgfyasp.hh>

  #include <dune/corepy/grid.hh>

  typedef YaspGrid<2, 2, Dune::EquidistantCoordinates> DuneType;

  PYBIND11_PLUGIN(grid_a1fb60a2a2b9998d3d2288f8c7c39128)
  {
    pybind11::module module("grid_a1fb60a2a2b9998d3d2288f8c7c39128")

    Dune::CorePy::registerGrid<DuneType>(module);

    return module.ptr();
  }

The first two lines are the two includes given in the dictionary and the typedef is constructed using the dictionary together with the named parameters in the call to `getModule`. The third include is for the file containing the register function (that implements the actual `pybind11` binding statements) `registerGrid`.

So the following class will be exported:

.. code-block:: c++
  
  pybind11::class_< DuneType >(module);

after the compilation of the above header file during the call to `getModule`. The actual shared library for the extension module (`grid_a1fb60a2a2b9998d3d2288f8c7c39128`) will be generated in `python/dune/generated/` within the cmake build directory tree. The name of the library is taken by hashing the Dune type of the implementation class.

Finally, you can now use your Python `yaspModule` module as described in :doc:`usage`!

Summary
===================================

The :py:class:`.generator.Generator` class is used to perform on the fly generation and import of extension modules for one given realization of a C++ interface class `InterfaceClass`. The details of each realization is provided through dictionaries contained in files within the directory `python/database/interfaceName` where `interfaceName` is the identifier used for the `InterfaceClass`. The
dictionary contains the means for constructing the correct type (`DuneType` in the above example) and a list of required include files. Calling the method :py:meth:`.Generator.getModule` with the identifier used in the dictionary for the desired implementation and the name parameters
required to fix `DuneType` results in a single header `python/dune/generated/generated_module.hh`. This file includes all headers provided in the dictionary plus a header `dune/corepy/interfaceName.hh` containing the definition of the pybind11 export. The extension library is built using the `cmake` build system with the `generate_module` target. The resulting library is renamed and imported into the python environment. 

.. note:: 
  The extension module will only be built if it does not exist
  already, i.e., no additional checks are performed to determine if
  the dependencies for this module have changed. 

.. _grid abstract base class: TODO 



.. _structure_developers:

***********************************
Structure of the codebase
***********************************

Now you know how the C++ code is exported to Python (especially with the database mechanism), you can understand the structure of `dune-corepy`: 

.. code-block:: text

    .
    ├── build-cmake
    ├── cmake
    │   └── modules
    │       ├── CMakeLists.txt
    │       ├── DuneCorepyMacros.cmake
    │       ├── DuneSphinxDoc.cmake
    │       └── FindSphinx.cmake
    ├── CMakeLists.txt
    ├── data
    │   ├── CMakeLists.txt
    │   ├── unitcube-1d.dgf
    │   ├── unitcube-2d.dgf
    │   └── unitcube-3d.dgf
    ├── demo
    │   ├── CMakeLists.txt
    │   ├── finiteelements.py
    │   ├── finitevolume.py
    │   ├── parfinitevolume.py
    │   └── shapefunctions.py
    ├── doc
    │   ├── CMakeLists.txt
    │   ├── doxygen
    │   │   ├── CMakeLists.txt
    │   │   └── Doxylocal
    │   └── sphinx
    │       ├── CMakeLists.txt
    │       ├── conf.py.in
    │       ├── developers.rst
    │       ├── index.rst
    │       └── usage.rst
    ├── dune
    │   ├── CMakeLists.txt
    │   └── corepy
    │       ├── CMakeLists.txt
    │       ├── common
    │       │   ├── CMakeLists.txt
    │       │   ├── common.cc
    │       │   ├── common.hh
    │       │   ├── densematrix.hh
    │       │   ├── densevector.hh
    │       │   ├── dynmatrix.hh
    │       │   ├── dynvector.hh
    │       │   ├── fmatrix.hh
    │       │   └── fvector.hh
    │       ├── function
    │       │   ├── CMakeLists.txt
    │       │   ├── gridfunctionview.hh
    │       │   └── simplegridfunction.hh
    │       ├── geometry
    │       │   ├── CMakeLists.txt
    │       │   ├── geometry.cc
    │       │   ├── multilineargeometry.hh
    │       │   ├── quadraturerules.hh
    │       │   ├── referenceelements.hh
    │       │   └── type.hh
    │       ├── grid
    │       │   ├── CMakeLists.txt
    │       │   ├── entity.hh
    │       │   ├── function.hh
    │       │   ├── geometry.hh
    │       │   ├── gridenums.hh
    │       │   ├── gridview.hh
    │       │   ├── hierarchical.hh
    │       │   ├── indexidset.hh
    │       │   ├── intersection.hh
    │       │   ├── mapper.hh
    │       │   ├── range.hh
    │       │   └── vtk.hh
    │       ├── grid.hh
    │       ├── istl
    │       │   └── CMakeLists.txt
    │       ├── localfunctions
    │       │   └── CMakeLists.txt
    │       └── pybind11
    ├── dune.module
    ├── python
    │   ├── CMakeLists.txt
    │   ├── database
    │   │   ├── CMakeLists.txt
    │   │   └── grid
    │   │       ├── CMakeLists.txt
    │   │       ├── dune-alugrid.db
    │   │       ├── dune-grid.db
    │   │       └── dune-spgrid.db
    │   └── dune
    │       ├── CMakeLists.txt
    │       ├── function.py
    │       ├── generated
    │       │   ├── CMakeLists.txt
    │       │   ├── generated_module.cc
    │       │   └── __init__.py
    │       ├── generator
    │       │   ├── CMakeLists.txt
    │       │   ├── database.py
    │       │   ├── generator.py
    │       │   └── __init__.py
    │       ├── grid.py
    │       ├── __init__.py
    │       └── mpihelper.cc
    └── README

* The **cmake/** directory contains some CMake modules for the build system.
* The DGF files that describe grids and any other data files are in **data/**
* The Python examples (from grid-howto.pdf) that use `dune-corepy` are in **demo/**
* You can find the sources of this documentation (powered by `sphinx`_) in **doc/**
* `dune/corepy/` contains the pybind11 binding statements to export the C++ code of DUNE to Python:

    * **common/**, **geometry/** and **grid/** respectively export some of the features of `dune-common`, `dune-geometry` and `dune-grid`
    * **pybind11/** contains all the header files of this library (since it is header-only)

* you can find the database mechanism described in the previous section in **python/** where **python/database/** contains the Python dictionaries, **python/dune/generator/** the `Generator` module and where **python/dune/generated/** is used to stock the generated extension modules. 

.. _sphinx: www.sphinx-doc.org/en/stable/index.html 


.. _technical_details_developers:

***********************************
Technical details
***********************************

This part will describe how some functionalities of the Dune interface have been exported to Python using pybind11. If you want to contribute to `dune_corepy` or are interested in implementing Python bindings for some other Dune modules, you may want to read this to see how some technical issues have been solved.


.. _iterators_developers:

Iterators
===================================

As described in `Iterating over grid entities and intersections`_, the Dune interface provides some `IteratorRange` that let you iterate over grid entities of your choice (eg. elements, facets, edges, vertices, etc.). For example, to iterate over the cells of your grid, you might write:

    .. code-block:: c++

        for (const auto& cell: elements(gridview))
        {
            // Do something
            ...
        }

Pybind11 defines the function `make_iterator` to provide Python iterators (from a C++ range) but unfortunately, `dune-corepy` does not use it since it was the source of some segmentation faults. Instead, the choice has been made to use a special data structure for exposing these kind of iterator types to Python (see the example `example-sequences-and-iterators.cpp`_ of the Pybind11 documentation).
Since a Python iterator is basically a class defining an `__iter__` and a `__next__` methods, the idea is to export a `pybind11::class_` that defines such methods with an underlying C++ data structure for implementing these methods.

For example, you can find the structure `PyGridViewIterator` in `dune/corepy/range.hh` that represents a generic iterator for grid entities of a specified codimension (and a specified grid view):

    .. code-block:: c++

        template<class GridView, int codim>
        struct PyGridViewIterator
        {
          typedef typename GridView::template Codim<codim>::Iterator Iterator;
          typedef typename GridView::template Codim<codim>::Entity Entity;

          PyGridViewIterator(const GridView& gridView)
            : it_(gridView.template begin<codim>()), end_(gridView.template end<codim>())
          {}

          Entity next()
          {
            if (it_ == end_)
              throw pybind11::stop_iteration();

            return \*it_++;
          }

        private:
          Iterator it_;
          Iterator end_;
        };

As you can see, it is just an iterator range defined by the two iterators `it_` and `end_`. This C++ data structure is then used in `registerPyGridViewIterator` (still in `dune/corepy/range.hh`) to export any iterator type (over entities of a (serial) grid view) to Python:

    .. code-block:: c++


        template<class GridView, int codim>
        void registerPyGridViewIterator(pybind11::handle scope, const char \*rgName)
        {
          typedef PyGridViewIterator<GridView, codim> Iterator;

          static const std::string itName = std::string(rgName) + "Iterator";
          pybind11::class_<Iterator>(scope, itName.c_str())
            .def("__iter__", [] (Iterator& it) -> Iterator& { return it; })
            .def("__next__", &Iterator::next);
        }

Then, it can be used like in the following listing, to provide Python iterators for grid entities:

    .. code-block:: c++

        registerPyGridViewIterator<GridView, 0>(scope, "elements");

        .def_property_readonly("elements",
            [] (const GridView& instance) {
                return PyGridViewIterator<GridView, 0>(instance);
            },
            pybind11::keep_alive<0, 1>());

Note that the use of `pybind11::keep_alive<0, 1>()` is important to bind the lifetime of your iterator with the lifetime of your grid view to ensure that the grid view will not be garbage collected before your iterator (to prevent some potential segmentation faults).

In this case, you can now use this attribute "elements" of a grid view to iterate over the elements (the cells) of that grid view:

    .. code-block:: python

        for cell in gridView.elements:
            # Do something
            ...


.. _jump_tables_developers:

Jump tables
===================================

When providing Python bindings for a C++ library, you may have to deal with template arguments: specified at compile-time in C++, you would like to set them at run-time in Python, like any function argument.
To provide such mechanism, `dune-corepy` implements the idea of a "jump table": when you know exactly what values can be assigned to your template argument, you can thus generate at compile-time a static array (a table) that contains all the possible specializations (defined by the range of values your template argument takes) of your function template. Then, you can finally export a Python function that takes a parameter corresponding to your template argument and map it to a function of your jump table.

Let's introduce you a concrete example that you can find in the codebase of `dune-corepy`. In fact, as described in `Iterating over grid entities and intersections`_, the Dune interface provides a function `entities` that takes a grid view `gv` and a codimension `cd` as parameters and returns an object representing the range of entities of codimension `cd` contained in the grid view `gv`. Here, `cd` is defined by its template parameter, see the following listing:

    .. code-block:: c++

        for (const auto& e: entities(gridview, Dune::Codim<0>())
        {
            // Do something
            ...
        }

And so, to provide such function in Python, `dune-corepy` uses a jump table, implemented in `dune/corepy/grid/gridview.hh`. 

As said before, this jump table is basically a static array. So, given a function `registerPyGridViewIterators` that returns this array, we can write:

    .. code-block:: c++

        static const auto pyGridViewIterators =
            registerPyGridViewIterators<GridView>(cls, std::make_integer_sequence<int, dim+1>());

and then use it as follows, to export the function `entities` to Python:

    .. code-block:: c++

        cls.def("entities",
            [] (const GridView& gridView, int codim) {
                return pyGridViewIterators[codim](gridView);
            },
            pybind11::keep_alive<0, 1>());

In this case, the template parameter `codim` is in the range {0, 1, ..., dim} and thus, the array `pyGridViewIterators` contains `dim+1` functions that manage the creation of iterators for each codimension. As this array is generated at compile-time, the `registerPyGridViewIterators` actually uses variadic templates and so the `std::make_integer_sequence<int, dim+1>()` argument to create a compile-time sequence of integers to represent all the possible values of the template
argument `codim`.

Note that the `static` keyword is essential to ensure the existence of the `pyGridViewIterators` array during the duration of your program.

Let's have a look at the `registerPyGridViewIterators` function:

    .. code-block:: c++

        template<class GridView, int codim>
        auto registerPyGridViewIterator(pybind11::handle scope)
        {
          static const std::string name = "entities" + std::to_string(codim);
          registerPyGridViewIterator<GridView, codim>(scope, name.c_str());

          std::function<pybind11::object(const GridView&)> funCreateIterator =
            [] (const GridView& gridView) {
              return pybind11::cast(PyGridViewIterator<GridView, codim>(gridView));
            };

          return funCreateIterator;
        }

        template<class GridView, int... codim>
        auto registerPyGridViewIterators(pybind11::handle scope, std::integer_sequence<int, codim...>)
        {
          std::array<std::function<pybind11::object(const GridView&)>, sizeof...(codim)> pyGridViewIterators =
            {registerPyGridViewIterator<GridView, codim>(scope)...};

          return pyGridViewIterators;
        }

As you can see, `registerPyGridViewIterators` is in fact a variadic template taking the parameter pack `codim` to represent the range of possible values for the codimension. As said before, the returned array contains functions that manage the creation of iterators. These functions have the signature `pybind11::object(const GridView&)` as they take a grid view as parameter and return a Python iterator (a Python object). The array is then generated by unpacking the
parameter pack `codim`, calling the `registerPyGridViewIterator` function for each value of `codim`, with `{registerPyGridViewIterator<GridView, codim>(scope)...}`.

The `registerPyGridViewIterator` function exports the iterator type to Python by calling the overload `registerPyGridViewIterator` in `dune/corepy/grid/range.hh` (see the above section :ref:`iterators_developers`) and then defines and returns the function `funCreateIterator` that manages the creation of iterators for the specified `codim`.

Note the `pybind11::cast` used to cast the `PyGridViewIterator<GridView, codim>` object to `pybind11::object` and which facilitates the writing of the signature of the function `funCreateIterator` (`std::function< pybind11::object(const GridView&) >`).


.. _wrappers_developers:

Wrappers
===================================

Sometimes, for some reasons, you cannot export directly a C++ type to Python using the classical `pybind11::class_<YourC++Type>(scope, name) ...`. In these cases, you may have to provide some thin wrapper of your C++ type to export it (undirectly) to Python.

For example, `dune-corepy` provide bindings for the `Dune::QuadratureRules<ctype, dim>` container that holds all quadrature rules of dimension `dim`, using a thin wrapper (in `dune/corepy/geometry/quadraturerules.hh`).

In fact, since `Dune::QuadratureRules` is a class template parameterized by `dim`, `dune-corepy` defines a jump table (see the above section :ref:`jump_tables_developers`) to export this type to Python and transform this compile-time parameter `dim` into a run-time parameter. See:

    .. code-block:: c++

        template<class ctype, int... dim>
        void registerQuadratureRules(pybind11::module module, std::integer_sequence<int, dim...>)
        {
          static const std::array< pybind11::object, sizeof...(dim) > quadratureRules =
            {registerQuadratureRules_<ctype, dim>(module)...};

          module.def("QuadratureRules", [](int dimension) { return quadratureRules[dimension]; });
        }

`quadratureRules` could be basically an array of `Dune::QuadratureRules` objects. Although, this type is actually a singleton that contains static methods and thus, does not have any public constructor. Unfortunately, we need a constructor to generate this array (since it is an array of instances of `Dune::QuadratureRules`) and this is this need that leads to a necessary wrapper of `Dune::QuadratureRules`.

So, instead of exporting `Dune::QuadratureRules` as following:

    .. code-block:: c++

    
        template<class ctype, int dim>
        auto registerQuadratureRules_(pybind11::handle scope)
        {
          typedef Dune::QuadratureRules<ctype, dim> QRules;

          static const std::string name = "QuadratureRules" + std::to_string(dim);
          pybind11::class_<QRules> cls(scope, name.c_str());

          cls.def_static("maxOrder", &QRules::maxOrder);

          cls.def_static("rule",
              [] (const GeometryType& t, int p) -> const Dune::QuadratureRule<ctype, dim>& {
                return QRules::rule(t, p);
              });

          cls.def_static("rule",
              [] (const GeometryType::BasicType& t, int p) -> const Dune::QuadratureRule<ctype, dim>& {
                return QRules::rule(t, p);
              });

          return pybind11::cast(QRules());
        }

that raises a compilation error "Dune::QuadratureRules ... is private within this context", `dune-corepy` just defines a wrapper `QuadratureRulesWrapper` used to be exported to Python instead of `Dune::QuadratureRules`:

    .. code-block:: c++

        template<class ctype, int dim>
        struct QuadratureRulesWrapper
        {
          typedef Dune::QuadratureRule<ctype, dim> QuadratureRule;

          QuadratureRulesWrapper() {}

          static unsigned maxOrder(const GeometryType& t,
              Dune::QuadratureType::Enum qt = Dune::QuadratureType::GaussLegendre)
          {
            return Dune::QuadratureRules<ctype, dim>::maxOrder(t, qt);
          }

          static const QuadratureRule& rule(const GeometryType& t, int p,
              Dune::QuadratureType::Enum qt = Dune::QuadratureType::GaussLegendre)
          {
            return Dune::QuadratureRules<ctype, dim>::rule(t, p, qt);
          }

          static const QuadratureRule& rule(const GeometryType::BasicType t, int p,
              Dune::QuadratureType::Enum qt = Dune::QuadratureType::GaussLegendre)
          {
            return Dune::QuadratureRules<ctype, dim>::rule(t, p, qt);
          }
        };

This wrapper is just a kind of proxy that forwards all the method calls to the real (implemented) methods of `Dune::QuadratureRules`. Since it provides a public constructor, we can finally generate our jump table just by modifying the `typedef` inside `registerQuadratureRules_`:

    .. code-block:: c++

        typedef QuadratureRulesWrapper<ctype, dim> QRules;

As you can see, it is finally this wrapper that is exposed to Python but using the underlying C++ type during the execution.


.. _conditional_exports_developers:

Conditional exports
===================================

When exporting a C++ class template, you may want to expose some methods only when some template parameters satisfy a condition.

For example, the `local` and `jacobianInverseTransposed` methods of the `Dune::MultiLinearGeometry<ct, mydim, cdim, Traits>` class template are only defined when `cdim >= mydim`.

In this case, you can use `std::conditional` to export these methods only when your template parameters satisfy your condition. So, for the case of `Dune::MultiLinearGeometry`, `dune-corepy` defines a structure `DoNothing`:

    .. code-block:: c++

        template<typename MGeometry>
        struct DoNothing
        {
          DoNothing() {}

          void operator()(pybind11::class_<MGeometry>&) {}
        };

and a structure `RegisterMemberFunctions`

    .. code-block:: c++

        template<typename MGeometry>
        struct RegisterMemberFunctions
        {
          RegisterMemberFunctions() {}

          void operator()(pybind11::class_<MGeometry>& cls)
          {
            cls.def("localPosition"            , &MGeometry::local);
            cls.def("jacobianInverseTransposed", &MGeometry::jacobianInverseTransposed);
          }
        };

which are used respectively when `cdim < mydim` and `cdim >= mydim`, in the function that exposes `Dune::MultiLinearGeometry` to Python:

    .. code-block:: c++

        template<class ctype, int mydim, int cdim>
        void registerMultiLinearGeometryType(pybind11::handle scope)
        {
          typedef MultiLinearGeometry<ctype, mydim, cdim> MGeometry;

          static const std::string name = "MultiLinearGeometry";
          pybind11::class_<MGeometry> cls(scope, name.c_str());

          ...

          std::conditional_t<(cdim >= mydim), RegisterMemberFunctions<MGeometry>, DoNothing<MGeometry> >{}(cls);

          ...
        }

.. _Iterating over grid entities and intersections: TODO
.. _example-sequences-and-iterators.cpp: https://github.com/pybind/pybind11/blob/master/example/example-sequences-and-iterators.cpp#L161
