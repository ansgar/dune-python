Welcome to dune-corepy's documentation!
=======================================

#################################
Introduction
#################################

.. toctree::
   :maxdepth: 2

This module aims to provide Python bindings for the `Dune`_ grid interface. It serves two main purposes:

1. High level program control for solving partial differential equations using the concise and clear syntax of Python but with the performances of C++ behind.

2. Rapid prototyping of new solutions and new implementations of `Dune`_ interfaces. 

.. _Dune: http://www.dune-project.org

#################################
Installation notes
#################################

Assuming you have installed the DUNE modules in a common directory `dune/`, go into that directory and clone the repository of `dune-corepy`:

.. code-block:: bash

    git clone https://gitlab.dune-project.org/michael.sghaier/dune-corepy.git

You can now compile `dune-corepy` like any other DUNE module, using `dunecontrol`:

.. code-block:: bash

    ./dune-common/bin/dunecontrol --only=dune-corepy --opts=your-opts-file all

Then, as said at the end of the compilation message, do not forget to type in your terminal:

.. code-block:: bash

    export PYTHONPATH="/path-to-your-dune-directory/dune/dune-corepy/build-cmake/python:$PYTHONPATH"

Finally, you can test the examples of the grid-howto.pdf that are in **dune-corepy/demo/**: go into the **build-cmake** directory of `dune-corepy`:

.. code-block:: bash

    cd dune-corepy/build-cmake/

Then, simply type:

.. code-block:: bash

    python demo/finitevolume.pyc

to run the finite volume scheme

.. code-block:: bash

    python demo/finiteelements.pyc

to run the finite elements scheme

and

.. code-block:: bash

    mpirun -np 2 python demo/parfinitevolume.pyc

to run the parallel finite volume on two processes, using OpenMPI.

If everything goes well, you should get some VTU files that you can visualize with ParaView.

#################################
Basic usage
#################################

.. toctree::
   :maxdepth: 1

   usage

#################################
Developer manual
#################################

.. toctree::
   :maxdepth: 1

   developers

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

