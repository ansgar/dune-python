#include <iostream>

#include <dune/corepy/pybind11/eval.h>

int main()
{
  Py_InitializeEx(0);
  auto global = pybind11::dict(pybind11::module::import("__main__").attr("__dict__"));
  {
    auto local = pybind11::dict();
    local["call_test"] = pybind11::cpp_function([&]() -> int {
              return 42;
    });
    auto result = pybind11::eval<pybind11::eval_statements>(
           "print('Hello World!');\n"
           "x = call_test();",
       global, local
    );
    auto x = local["x"].cast<int>();
    if (! ( result == pybind11::none() && x == 42 ) )
      std::cout << "Test 1 failed" << std::endl;
  }
  {
    auto local = pybind11::dict();
    local["x"] = pybind11::int_(42);
    auto x = pybind11::eval("x", global, local);
    if (! ( x.cast<int>() == 42 ) )
      std::cout << "Test 2 failed" << std::endl;
  }
  {
    auto local = pybind11::dict();
    local["call_test"] = pybind11::cpp_function([&]() -> int {
             return 42;
    });
    auto result = pybind11::eval<pybind11::eval_single_statement>("x = call_test()", pybind11::dict(), local);
    auto x = local["x"].cast<int>();
    if ( ! ( result == pybind11::none() && x == 42 ) )
      std::cout << "Test 3 failed" << std::endl;
  }
  {
    std::string filename = "test_eval_call.py";
    auto local = pybind11::dict();
    local["y"] = pybind11::int_(43);
    int val_out;
    local["call_test2"] = pybind11::cpp_function([&](int value) { val_out = value; });
    auto result = pybind11::eval_file(filename, global, local);
    if ( ! ( val_out == 43 && result == pybind11::none() ) )
      std::cout << "Test 4 failed" << std::endl;
  }
  {
    try {
      pybind11::eval("nonsense code ...");
    } catch (pybind11::error_already_set &) {
      std::cout << "nonsense FAILS!" << std::endl;
    }
    try {
      pybind11::eval_file("non-existing file");
    } catch (std::exception &) {
      std::cout << "it really does" << std::endl;
    }
  }
  Py_Finalize();
}
