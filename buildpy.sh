git clone -b releases/2.5 https://gitlab.dune-project.org/core/dune-common.git
git clone -b releases/2.5 https://gitlab.dune-project.org/core/dune-geometry.git
git clone -b releases/2.5 https://gitlab.dune-project.org/core/dune-grid.git
git clone -b releases/2.5 https://gitlab.dune-project.org/extensions/dune-alugrid.git
git clone -b master https://gitlab.dune-project.org/staging/dune-corepy.git

CMAKE_FLAGS=" \
  -DBUILD_SHARED_LIBS=TRUE \
  -DCMAKE_BUILD_TYPE=Release \
  -DDUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS=TRUE \
  -DENABLE_HEADERCHECK=OFF \
  -DCMAKE_DISABLE_FIND_PACKAGE_LATEX=TRUE \
  -DCMAKE_DISABLE_DOCUMENTATION=TRUE \
"
echo "CMAKE_FLAGS=\"${CMAKE_FLAGS}\"" > config.opts

./dune-common/bin/dunecontrol --opts=config.opts all

export DUNE_CMAKE_FLAGS=${CMAKE_FLAGS}
export DUNE_CONTROL_PATH=${PWD}
export PYTHONPATH=${PWD}/dune-corepy/build-cmake/python:${PWD}/dune-alugrid/build-cmake/python

cd dune-corepy/demo
python test.py

echo "Set the following environment variables, i.e., for bash"
echo "  export DUNE_CONTROL_PATH=${DUNE_CONTROL_PATH}"
echo "  export PYTHONPATH=${PYTHONPATH}"
echo "Then try out"
echo "  python grid-demo.py"
echo "in dune-corepy/demo or have a look at the jupyter notebooks in dune-corepy/notebooks"
