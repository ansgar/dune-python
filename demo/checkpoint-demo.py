from __future__ import print_function

import math
import sys

from argparse import ArgumentParser

from dune.common import pickle
from dune.grid import cartesianDomain

import dune.create as create

parser = ArgumentParser(description='Checkpointing demo for dune-corepy')
parser.add_argument('-g', '--grid', help='type of grid')
parser.add_argument('-c', '--cells', type=int, nargs=2, help='number of cells in each direction')
parser.add_argument('-l', '--lower', type=float, nargs=2, help='lower left corner', default=[0.0, 0.0])
parser.add_argument('-u', '--upper', type=float, nargs=2, help='upper right corner', default=[1.0, 1.0])
parser.add_argument('-i', '--input', help='input file name')
parser.add_argument('-o', '--output', help='output file name')
parser.add_argument('-f', '--format', help='output format: either "pickle" or "vtk"', default="pickle")
parser.add_argument('-r', '--refine', type=int, help='globally refine grid n times')

try:
    args = parser.parse_args()
except Exception as e:
    print(e)
    sys.exit(1)

if args.input and (args.grid or args.cells):
    print("Error: When using an input file, you can specify neither grid nor domain")
    sys.exit(1)

if not args.input and not (args.grid and args.cells):
    print("Error: You must specify either an input file or a grid and a domian")
    sys.exit(1)

if args.cells:
    domain = cartesianDomain(args.lower, args.upper, args.cells)
    grid = create.grid(args.grid, domain, dimgrid=2)

if args.input:
    grid = pickle.load(open(args.input, 'rb'))

if args.refine:
    grid.hierarchicalGrid.globalRefine(args.refine)

if args.output:
    if args.format == "pickle":
        pickle.dump(grid, open(args.output, 'wb'), 2)
    elif args.format == "vtk":
        grid.vtkWriter().write(args.output)
    else:
        print("Error: Invalid output format.")
        sys.exit(1)
