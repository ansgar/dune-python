from __future__ import print_function

import math

import dune.common as common
import dune.grid as grid
from dune.alugrid import aluCubeGrid

domain = grid.cartesianDomain([0, 0], [1, 1], [10, 10])
grid = aluCubeGrid(domain, dimgrid=2)

t = 0
maxLevel = 4
hgrid = grid.hierarchicalGrid
marker = common.Marker
vtk = grid.vtkWriter()

def mark(element):
    y = element.geometry.center - [0.5+0.2*math.cos(t), 0.5+0.2*math.sin(t)]
    if y.two_norm2 < 0.2*0.2 and y.two_norm2 > 0.1*0.1:
      return marker.refine if element.level < maxLevel else marker.keep
    else:
      return marker.coarsen

for i in range(0,maxLevel):
    hgrid.mark(mark)
    hgrid.adapt()
    hgrid.loadBalance()

nr = 0
while t < 2*math.pi:
    print('time:',t)
    hgrid.mark(mark)
    hgrid.adapt()
    hgrid.loadBalance()
    vtk.write("adapt-demo", nr)
    print(grid.size(0))
    t = t+0.1
    nr = nr+1
