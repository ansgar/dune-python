from __future__ import print_function

# needed on some machines
from mpi4py import MPI

import math

# dune modules
import dune.common as common
import dune.grid as grid
import dune.function as function
import dune.create as create

###################################################

# just get the grid (only for testing - not used)
# onedgrid = grid.oneDGrid("../data/unitcube-1d.dgf")
#onedgrid = grid.create("OneD", "../data/unitcube-1d.dgf")
onedgrid = create.grid("OneD", "../data/unitcube-1d.dgf")

for element in onedgrid.elements():
    print( "Center ", element.geometry.center )
    for corner in element.geometry.corners:
        print( "Corner ", corner )

# get the full grid module and then the grid (module needed for grid # functions and output object)
yaspgrid = create.grid("Yasp", "../data/unitcube-2d.dgf", dimgrid=2)
yaspgrid.hierarchicalGrid.globalRefine(2)

vtk_yaspgrid = yaspgrid.vtkWriter()

def expr_global(x):
    return [-(x[1] - 0.5)*math.sin(x[0]*12)]

ggf = yaspgrid.globalGridFunction(expr_global)
ggf.addToVTKWriter("expr_global", vtk_yaspgrid, common.DataType.PointData)
lf = ggf.localFunction()
for element in yaspgrid.elements():
    lf.bind(element)
    x = [0.5, 0.5]
    y = element.geometry.position(x)
    print("ggf( ", y, " ) = ", lf(x), " | ", expr_global(y))
    lf.unbind()

def expr_local(element, x):
    geo = element.geometry
    return [abs(expr_global(geo.position(x))[0] - expr_global(geo.center)[0]),
            expr_global(geo.position(x))[0]]

lgf = yaspgrid.localGridFunction(expr_local)
lgf.addToVTKWriter("expr_local", vtk_yaspgrid, common.DataType.PointData)
lf = lgf.localFunction()
for element in yaspgrid.elements():
    lf.bind(element)
    x = [0.5, 0.5]
    y = element.geometry.position(x)
    print("lgf( ", y, " ) = ", lf(x), " | ", expr_local(element,x))
    lf.unbind()

ggf = yaspgrid.globalGridFunction(function.MathExpression(["-(x1-0.5)","x0-1./2.","x0","x1*x1","x0*x1","math.sin(x0*x1)","math.exp(-(x0-0.5)**2)"]))
ggf.addToVTKWriter("MathExpression", vtk_yaspgrid, common.DataType.PointData)
lf = ggf.localFunction()
for element in yaspgrid.elements():
    lf.bind(element)
    x = [0.5, 0.5]
    y = element.geometry.position(x)
    print("ggf( ", y, " ) = ", lf(x))
    lf.unbind()

class ExprLocal:
  def __init__(self,gf):
    self.gf_ = gf
  def __call__(self,element, x):
    geo = element.geometry
    return [abs(self.gf_(geo.position(x))[0] - self.gf_(geo.center)[0]),
            self.gf_(geo.position(x))[0]]
lgf = yaspgrid.localGridFunction(ExprLocal(expr_global))
lgf.addToVTKWriter("ExprLocal", vtk_yaspgrid, common.DataType.PointData)
lf = lgf.localFunction()
for element in yaspgrid.elements():
    lf.bind(element)
    x = [0.5, 0.5]
    y = element.geometry.position(x)
    print("lgf( ", y, " ) = ", lf(x), " | ", expr_local(element,x))
    lf.unbind()

# remove all variables in python to make sure they stay alive long enough for the vtk # writer
lgf = "null"
ggf  ="null"

vtk_yaspgrid.write("yaspgrid_demo");

######################################################

try:
    import numpy
    from scipy.spatial import Delaunay
    import matplotlib.pyplot as plt
    radii = numpy.linspace(1.0 / n_radii, 1.0, n_radii)
    angles = numpy.linspace(0, 2*numpy.pi, n_angles, endpoint=False)
    angles = numpy.repeat(angles[..., numpy.newaxis], n_radii, axis=1)
    x = numpy.append(0, (radii*numpy.cos(angles)).flatten())
    y = numpy.append(0, (radii*numpy.sin(angles)).flatten())
    points = numpy.stack((x,y), axis=-1)
    triangles = Delaunay(points).simplices
    plt.triplot(points[:,0], points[:,1], triangles.copy())
    plt.plot(points[:,0], points[:,1], 'o')
    plt.show()
    alugrid = create.grid("ALUConform", {'vertices':points, 'simplices':triangles}, dimgrid=2)
except:
    alugrid = create.grid("ALUConform", "../data/unitcube-2d.dgf", dimgrid=2)

output = alugrid.vtkWriter()
output.write("alugrid_demo")

from dune.alugrid import aluConformGrid
surface = aluConformGrid("../data/sphere.dgf", dimgrid=2, dimworld=3)
ggf = surface.globalGridFunction(lambda x: [ math.atan2(x[1],x[0]) ])
surface.writeVTK("surface_demo", celldata={"angle" : ggf})
