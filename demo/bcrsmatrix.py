from dune.istl import BCRSMatrix

mat = BCRSMatrix((5, 5), 5, BCRSMatrix.random)
print(mat.shape, mat.nonZeroes, mat.buildMode)

for i in range(5):
    mat.setRowSize(i, 1)
mat.endRowSizes()

for i in range(5):
    mat.setIndices(i, [i])
mat.endIndices()

mat.store("bcrsmatrix.mm", "matrixmarket")
mat.load("bcrsmatrix.mm", "matrixmarket")
mat.store("bcrsmatrix.txt", "matlab")
