import numpy as np
from mpi4py import MPI

import dune.common as common
import dune.grid as grid
from dune.grid.map import MultipleCodimMultipleGeomTypeMapper as MCMGMapper

import dune.create as create

def c0(x):
    if x.two_norm > 0.125 and x.two_norm < 0.5:
        return 1.0
    else:
        return 0.0

def b(x, t):
    return 0.0

def u(x, t):
    return [1.0, 1.0]

def initialize(gridView, mapper):
    c = np.zeros(len(mapper))

    for e in gridView.entities(0):
        center = e.geometry.center # global coordinate of cell center
        c[mapper.index(e)] = c0(center) # initialize cell concentration

    return c

class DataHandle:
    def __init__(self, mapper, vector):
        self.mapper = mapper
        self.vector = vector

    def contains(self, dim, codim):
        return codim == 0

    def fixedsize(self, dim, codim):
        return True

    def size(self, entity):
        return 1

    def gather(self, entity):
        return [self.vector[self.mapper.index(entity)]]

    def scatter(self, entity, data):
        self.vector[self.mapper.index(entity)] = data[0]

def evolve(gridView, mapper, concentration, t):
    update = np.zeros(len(concentration))
    dt = 1e100

    for entity in gridView.elements(common.PartitionIteratorType.All):
        volume    = entity.geometry.volume
        indexi    = mapper.index(entity)
        sumfactor = 0.0

        for intersection in gridView.intersections(entity):
            igeo = intersection.geometry

            faceglobal = igeo.center

            normalvelocity = (u(faceglobal, t) * intersection.centerUnitOuterNormal) * igeo.volume

            factor = normalvelocity / volume

            if factor > 0:
                sumfactor += factor

            outside = intersection.outside
            if outside is not None:
                indexj = mapper.index(outside)

                if indexi < indexj:
                    nbfactor = normalvelocity / outside.geometry.volume

                    # upwind concentration
                    c = concentration[indexj if factor < 0 else indexi]

                    update[indexi] -= c * factor
                    update[indexj] += c * nbfactor

            elif intersection.boundary:
                c = b(faceglobal, t) if factor < 0 else concentration[indexi]
                update[indexi] -= c * factor

        if entity.partitionType == common.PartitionType.Interior:
            dt = min(dt, 1.0 / sumfactor)

    dt = 0.99 * gridView.comm.min(dt)

    gridView.communicate(DataHandle(mapper, update), common.InterfaceType.InteriorBorder_All, common.CommunicationDirection.Forward)

    concentration += update*dt # concentration vector updated by side effect

    return dt


if __name__ == "__main__":
    domain = grid.cartesianDomain([0, 0], [1, 1], [8, 8, 8], overlap=1)
    gridView = create.grid("Yasp", domain, dimgrid=2)

    gridView.hierarchicalGrid.globalRefine(2)
    gridView.hierarchicalGrid.loadBalance()

    mapper = MCMGMapper(gridView, lambda gt: gt.dim == gridView.dimension)

    concentration = initialize(gridView, mapper)

    vtkwriter = gridView.vtkWriter()
    lgf = gridView.localGridFunction(lambda element, _: [concentration[mapper.index(element)]])
    lgf.addToVTKWriter("concentration", vtkwriter, common.DataType.CellData)

    counter = 0
    t, dt = 0.0, 0.0
    k = 0
    saveInterval = 0.1
    saveStep = 0.1

    vtkwriter.write("concentration", counter, common.OutputType.appendedraw)
    counter += 1

    while t < 0.5:
        k += 1

        dt = evolve(gridView, mapper, concentration, t)
        if gridView.hierarchicalGrid.comm.rank == 0:
            print("k=" + str(k) + " ; dt=" + str(dt))

        t += dt
        if t >= saveStep:
            vtkwriter.write("concentration", counter, common.OutputType.appendedraw)
            saveStep += saveInterval
            counter += 1
