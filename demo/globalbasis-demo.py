import dune.create as create

from dune.grid import cartesianDomain
from dune.functions import *

domain = cartesianDomain([0, 0], [1, 1], [8, 8])
yaspgrid = create.grid("Yasp", domain, dimgrid=2)

print("nodes:", yaspgrid.size(2), "edges:",yaspgrid.size(1), "elements:", yaspgrid.size(0))

basis = create.globalBasis("default", yaspgrid, Lagrange(2))
print(str(Lagrange(2)) + ".dimension:", basis.dimension, str(Lagrange(2)) + ".dimRange:", basis.dimRange)

basis = create.globalBasis("default", yaspgrid, Lagrange(1,2))
print(str(Lagrange(1,2)) + ".dimension:", basis.dimension, str(Lagrange(1,2)) + ".dimRange:", basis.dimRange)

basis = create.globalBasis("default", yaspgrid, Power(Lagrange(1),2))
print(str(Power(Lagrange(1),2)) + ".dimension:", basis.dimension, str(Power(Lagrange(1),2)) + ".dimRange", basis.dimRange)

taylorHood = Lagrange(2)**2 * Lagrange(1)
taylorHoodBasis = create.globalBasis("default", yaspgrid, taylorHood)
print("nodes*3 + edges*2 + elements*2:", yaspgrid.size(2)*3 + yaspgrid.size(1)*2 + yaspgrid.size(0)*2)
print(str(taylorHood) + ".dimension:", taylorHoodBasis.dimension, str(taylorHood) + ".dimRange:", taylorHoodBasis.dimRange)

import numpy

dofVector = numpy.zeros(taylorHoodBasis.dimension);
zero = taylorHoodBasis.asFunction(dofVector)
yaspgrid.writeVTK("globalbasis-demo", pointdata={"zero": zero})
