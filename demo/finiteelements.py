import numpy
from scipy.sparse import coo_matrix
from scipy.sparse.linalg import spsolve

from dune.grid import cartesianDomain
from dune.grid import ugGrid


class LinearShapeFunction:
    def __init__(self, ofs, grad):
        self.ofs = ofs
        self.grad = grad
    def evaluate(self, local):
        return self.ofs + sum([x*y for x, y in zip(self.grad, local)])
    def gradient(self, local):
        return self.grad


def P1ShapeFunctionSet(dim):
    sfs = [LinearShapeFunction(1.0, [-1.0] * dim)]
    for i in range(dim):
        sfs.append(LinearShapeFunction(0.0, [float(i == j) for j in range(dim)]))
    return sfs


def assemble(gridView, f):
    dim = gridView.dimension

    basis = P1ShapeFunctionSet(dim)
    indexSet = gridView.indexSet

    value, rowIndex, colIndex = [], [], []
    rhs = numpy.zeros(indexSet.size(dim))
    for e in gridView.elements():
        geo = e.geometry

        gt  = e.type
        if not gt.isSimplex:
            raise NotImplementedError("finite element shape functions only implemented for simplex elements")

        for p in gridView._module.quadratureRule(gt, 1):
            x = p.position
            w = p.weight * geo.integrationElement(x)

            jit = numpy.array(geo.jacobianInverseTransposed(x), copy=False)
            grads = [numpy.dot(jit, phi.gradient(x)) for phi in basis]

            for i, dphi in enumerate(grads):
                row = indexSet.subIndex(e, i, dim)
                for j, dpsi in enumerate(grads):
                    value.append(numpy.dot(dphi, dpsi) * w)
                    rowIndex.append(row)
                    colIndex.append(indexSet.subIndex(e, j, dim))

        for p in gridView._module.quadratureRule(gt, 2):
            x = p.position
            w = p.weight * geo.integrationElement(x)
            for i, phi in enumerate(basis):
                rhs[indexSet.subIndex(e, i, dim)] += phi.evaluate(x) * f(geo.position(x)) * w

    matrix = coo_matrix((value, (rowIndex, colIndex))).tocsr()

    # Dirichlet boundary conditions

    drows = set()
    for e in gridView.elements():
        if not e.hasBoundaryIntersections:
            continue
        for i in gridView.intersections(e):
            if not i.boundary:
                continue
            ref = e.geometry.domain
            j = i.indexInInside
            subs = [ref.subEntity(j, 1, k, dim) for k in range(ref.size(j, 1, dim))]
            drows.update(indexSet.subIndex(e, k, dim) for k in subs)

    drows = numpy.array(list(drows))
    for r in drows:
        matrix.data[matrix.indptr[r]:matrix.indptr[r+1]] = 0.0
        rhs[r] = 0.0

    d = matrix.diagonal()
    d[drows] = 1.0
    matrix.setdiag(d)

    return matrix, rhs

domain = {'vertices': numpy.array([(0.0, 0.0), (1.0, 0.0), (1.0, 1.0), (0.0, 1.0)]), 'simplices': numpy.array([(0, 1, 2), (0, 2, 3)])}
gridView = ugGrid(domain, dimgrid=2)
gridView.hierarchicalGrid.globalRefine(8)

print("Assembling...")
matrix, rhs = assemble(gridView, lambda v: sum(2.0 * x * (1 - x) for x in v))

print("Solving...")
u = spsolve(matrix, rhs)

print("Visualizing...")
dim = gridView.dimension
basis = P1ShapeFunctionSet(dim)
indexSet = gridView.indexSet
evaluate_u = lambda e, x: [sum(phi.evaluate(x) * u[indexSet.subIndex(e, i, dim)] for i, phi in enumerate(basis))]
lgf = gridView.localGridFunction(evaluate_u)
gridView.writeVTK("fem2d", pointdata={"u": lgf})
