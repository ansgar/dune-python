// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <vector>

#include <dune/common/function.hh>
#include <dune/common/bitsetvector.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/taylorhoodbasis.hh>
#include <dune/functions/functionspacebases/hierarchicvectorwrapper.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/subspacebasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/preconditioners.hh>

using namespace Dune;

template< class LocalView, class ElementMatrix >
void getLocalMatrix ( const LocalView &localView, ElementMatrix &elementMatrix )
{
  typedef typename LocalView::Element Element;
  const Element& element = localView.element();

  const int dim = Element::dimension;
  auto geometry = element.geometry();

  // elementMatrix.setSize(localView.size(), localView.size());
  // elementMatrix = 0;      // fills the entire matrix with zeroes
  for (int i=0;i<localView.size();++i)
    for (int j=0;j<localView.size();++j)
      *(double*)( elementMatrix.data(i,j) ) = 0.;

  using namespace Dune::TypeTree::Indices;
  const auto& velocityLocalFiniteElement = localView.tree().child(_0,0).finiteElement();  /*@\label{li:stokes_taylorhood_get_velocity_lfe}@*/
  const auto& pressureLocalFiniteElement = localView.tree().child(_1).finiteElement();    /*@\label{li:stokes_taylorhood_get_pressure_lfe}@*/

  int order = 2*(dim*velocityLocalFiniteElement.localBasis().order()-1);
  const auto& quad = QuadratureRules<double, dim>::rule(element.type(), order);

  for (const auto& quadPoint : quad)
  {
    const auto jacobian = geometry.jacobianInverseTransposed(quadPoint.position());

    const double integrationElement = geometry.integrationElement(quadPoint.position());

    std::vector<FieldMatrix<double,1,dim> > referenceGradients;
    velocityLocalFiniteElement.localBasis().evaluateJacobian(quadPoint.position(), referenceGradients);

    std::vector<FieldVector<double,dim> > gradients(referenceGradients.size());
    for (size_t i=0; i<gradients.size(); i++)
      jacobian.mv(referenceGradients[i][0], gradients[i]);

    for (size_t i=0; i<velocityLocalFiniteElement.size(); i++)
      for (size_t j=0; j<velocityLocalFiniteElement.size(); j++ )
        for (size_t k=0; k<dim; k++)
        {
          size_t row = localView.tree().child(_0,k).localIndex(i);                    /*@\label{li:stokes_taylorhood_compute_vv_element_matrix_row}@*/
          size_t col = localView.tree().child(_0,k).localIndex(j);                    /*@\label{li:stokes_taylorhood_compute_vv_element_matrix_column}@*/
          *(double*)( elementMatrix.data(row,col) ) += ( gradients[i] * gradients[j] ) * quadPoint.weight() * integrationElement;  /*@\label{li:stokes_taylorhood_update_vv_element_matrix}@*/
          // elementMatrix[row][col] += ( gradients[i] * gradients[j] ) * quadPoint.weight() * integrationElement;  /*@\label{li:stokes_taylorhood_update_vv_element_matrix}@*/
        }

    std::vector<FieldVector<double,1> > pressureValues;
    pressureLocalFiniteElement.localBasis().evaluateFunction(quadPoint.position(), pressureValues);

    for (size_t i=0; i<velocityLocalFiniteElement.size(); i++)
      for (size_t j=0; j<pressureLocalFiniteElement.size(); j++ )
        for (size_t k=0; k<dim; k++)
        {
          size_t vIndex = localView.tree().child(_0,k).localIndex(i);                      /*@\label{li:stokes_taylorhood_compute_vp_element_matrix_row}@*/
          size_t pIndex = localView.tree().child(_1).localIndex(j);                        /*@\label{li:stokes_taylorhood_compute_vp_element_matrix_column}@*/

          *(double*)( elementMatrix.data(vIndex,pIndex) )
             += gradients[i][k] * pressureValues[j] * quadPoint.weight() * integrationElement;
          *(double*)( elementMatrix.data(pIndex,vIndex) )
             += gradients[i][k] * pressureValues[j] * quadPoint.weight() * integrationElement;
          // elementMatrix[vIndex][pIndex] += gradients[i][k] * pressureValues[j] * quadPoint.weight() * integrationElement;  /*@\label{li:stokes_taylorhood_update_vp_element_matrix_a}@*/
          // elementMatrix[pIndex][vIndex] += gradients[i][k] * pressureValues[j] * quadPoint.weight() * integrationElement;  /*@\label{li:stokes_taylorhood_update_vp_element_matrix_b}@*/
        }
  }
}
#ifdef USING_COREPY
#include <dune/corepy/pybind11/pybind11.h>
#include <dune/corepy/pybind11/numpy.h>

template< class LocalView >
void assemble ( LocalView &localView, pybind11::array values )
{
  getLocalMatrix(localView,values);
}

template< class Basis >
std::pair< std::vector< int >, std::vector< double > > boundary ( Basis &basis, pybind11::function bnd )
{
  using namespace Dune::TypeTree::Indices;
  using Coordinate = typename Basis::GridView::template Codim<0>::Geometry::GlobalCoordinate;

  auto boundaryIndicator = [ bnd ] ( Coordinate x ) -> bool { return bnd( x ).template cast< bool >(); };

  typedef FieldVector< double, Basis::GridView::dimensionworld+1 > Range;
  auto &&dirichletData = [] ( Coordinate x )->Range { return { 0.0, double( x[ 0 ] < 1e-8 ), 0.0 }; };

  using BitVectorType = BlockVector< FieldVector< char, 1 > >;
  using HierarchicBitVectorView = Functions::HierarchicVectorWrapper< BitVectorType, char >;
  BitVectorType isBoundary;
  interpolate( basis, HierarchicBitVectorView( isBoundary ), boundaryIndicator );

  typedef BlockVector< FieldVector<double,1> > VectorType;
  VectorType rhs;
  typedef Dune::Functions::HierarchicVectorWrapper<VectorType, double> HierarchicVectorView;
  HierarchicVectorView( rhs ).resize( basis );
  rhs = 0;

  interpolate(basis, HierarchicVectorView(rhs), dirichletData, HierarchicBitVectorView(isBoundary));
  std::vector< int > rows;
  std::vector< double > values;
  const std::size_t numVelocityDofs = Functions::subspaceBasis( basis, _0 ).size();
  for( std::size_t i=0; i < numVelocityDofs; ++i )
  {
    if( !isBoundary[ i ] )
      continue;

    rows.push_back( i );
    values.push_back( rhs[ i ] );
  }
  return std::make_pair( rows, values );
}

#endif // #ifdef USING_COREPY
