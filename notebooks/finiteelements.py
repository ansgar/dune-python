# coding: utf-8

# # Finite Elements
#
# As another example we solve the poisson equation
#
# \begin{align*}
#     -\Delta u &= f && \text{in $\Omega$}, \\
#     u &= 0 && \text{auf $\partial\Omega$}
# \end{align*}
#
# in Python based on a simplicial Dune grid: `ALUConformGrid`.

# In[1]:

import time
import numpy
import math


# As a first step, we construct the grid:

# In[2]:

from dune.grid import cartesianDomain
from dune.alugrid import aluConformGrid
vertices = numpy.array([(0,0), (1,0), (1,1), (0,1), (-1,1), (-1,0), (-1,-1), (0,-1)])
triangles = numpy.array([(2,0,1), (0,2,3), (4,0,3), (0,4,5), (6,0,5), (0,6,7)])
aluView = aluConformGrid({"vertices": vertices, "simplices": triangles}, dimgrid=2)
aluView.hierarchicalGrid.globalRefine(7)


# In[3]:

class LinearShapeFunction:
    def __init__(self, ofs, grad):
        self.ofs = ofs
        self.grad = grad
    def evaluate(self, local):
        return self.ofs + sum([x*y for x, y in zip(self.grad, local)])
    def gradient(self, local):
        return self.grad

dim = aluView.dimension
p1ShapeFunctionSet = [LinearShapeFunction(1.0, [-1.0] * dim)]
for i in range(dim):
    p1ShapeFunctionSet.append(LinearShapeFunction(0.0, [float(i == j) for j in range(dim)]))


# Let's get going. First we assemble the right hand side with
# $$
#     f(x) = 2\prod_{i} x_i\,(1-x_i)
# $$

# In[4]:

f = lambda v: sum(2.0 * x * (1 - x) for x in v)

dim = aluView.dimension
indexSet = aluView.indexSet
rhs = numpy.zeros(indexSet.size(dim))

for e in aluView.elements():
    geo = e.geometry
    for p in aluView._module.quadratureRule(e.type, 2):
        x = p.position
        w = p.weight * geo.integrationElement(x)
        for i, phi in enumerate(p1ShapeFunctionSet):
            index = indexSet.subIndex(e, i, dim)
            rhs[index] += w * phi.evaluate(x) * f(geo.position(x))


# In[5]:

dim, indexSet = aluView.dimension, aluView.indexSet
value, rowIndex, colIndex = [], [], []
for e in aluView.elements():
    geo = e.geometry
    for p in aluView._module.quadratureRule(e.type, 1):
        x = p.position
        w = p.weight * geo.integrationElement(x)

        jit = numpy.array(geo.jacobianInverseTransposed(x), copy=False)
        grads = [numpy.dot(jit, phi.gradient(x)) for phi in p1ShapeFunctionSet]

        for i, dphi in enumerate(grads):
            row = indexSet.subIndex(e, i, dim)
            for j, dpsi in enumerate(grads):
                value.append(numpy.dot(dphi, dpsi) * w)
                rowIndex.append(row)
                colIndex.append(indexSet.subIndex(e, j, dim))

from scipy.sparse import coo_matrix
matrix = coo_matrix((value, (rowIndex, colIndex))).tocsr()


# In[6]:

rows = set()
for e in aluView.elements():
    for i in aluView.intersections(e):
        if not i.boundary:
            continue
        ref = e.geometry.domain
        j = i.indexInInside
        subs = [ref.subEntity(j, 1, k, dim) for k in range(ref.size(j, 1, dim))]
        rows.update(indexSet.subIndex(e, k, dim) for k in subs)


# ... and patch the corresponding rows in the linear system:

# In[7]:

rows = numpy.array(list(rows))
for r in rows:
    matrix.data[matrix.indptr[r]:matrix.indptr[r+1]] = 0.0
    rhs[r] = 0.0

d = matrix.diagonal()
d[rows] = 1.0
matrix.setdiag(d)


# In[8]:

from scipy.sparse.linalg import spsolve
u = spsolve(matrix, rhs)


# To visualize the result, we need to construct a corresponding grid function:

# In[9]:

evaluate_u = lambda e, x: [sum(phi.evaluate(x) * u[indexSet.subIndex(e, i, dim)]
                           for i, phi in enumerate(p1ShapeFunctionSet))]
lgf = aluView.localGridFunction(evaluate_u)

aluView.writeVTK("fem2d", pointdata={"u": lgf})


# In[10]:

from dune.plotting import plotPointData
plotPointData(lgf, figsize=(9,9), gridLines=None)
