#include <cstddef>
#include <limits>
#include <stdexcept>

#include <dune/common/fvector.hh>

#ifdef USING_COREPY
#include <dune/corepy/pybind11/pybind11.h>
#include <dune/corepy/pybind11/numpy.h>
#endif

template< class GridView, class M, class V, class Bnd >
double evolve ( const GridView &gridView, const M& mapper, V& c, const Bnd &bnd, double t )
{
  // type used for coordinates in the grid
  typedef typename GridView::ctype ct;

  static const int dimworld = GridView::dimensionworld;
  typedef typename GridView::template Codim< 0 >::Entity Entity;

  // intersection iterator type
  typedef typename GridView::Intersection Intersection;

  std::size_t size = gridView.size( 0 );
  // allocate a temporary vector for the update
  double update[size];
  for (std::size_t i=0; i<size; i++) update[i] = 0;

  // initialize dt very large
  double dt = std::numeric_limits< double >::max();

  // compute update vector and optimum dt in one grid traversal
  for( const Entity &entity : elements( gridView ) ) /*@\label{evh:loop0}@*/
  {
    // cell geometry
    const auto geo = entity.geometry();

    // cell volume, assume linear map here
    double volume = geo.volume();

    // cell index
    int indexi = mapper.index( entity );

    // variable to compute sum of positive factors
    double sumfactor = 0.0;

    // run through all intersections with neighbors and boundary
    for( const Intersection &intersection : intersections( gridView, entity ) ) /*@\label{evh:flux0}@*/
    {
      // get geometry type of face
      const auto igeo = intersection.geometry();

      // get normal vector scaled with volume
      Dune::FieldVector<ct,dimworld> integrationOuterNormal
        = intersection.centerUnitOuterNormal();
      integrationOuterNormal *= igeo.volume();

      // evaluate velocity at face center
      Dune::FieldVector<double,dimworld> velocity(1);

      // compute factor occuring in flux formula
      double factor = velocity*integrationOuterNormal/volume;

      // for time step calculation
      if (factor>=0) sumfactor += factor;

      // handle interior face
      if (intersection.neighbor())             // "correct" version /*@\label{evh:neighbor}@*/
      {
        // access neighbor
        const Entity outside = intersection.outside();
        int indexj = mapper.index(outside);

        // compute flux from one side only
        if (indexi<indexj)
        {
          // compute factor in neighbor
          const auto nbgeo = outside.geometry();
          double nbvolume = nbgeo.volume();
          double nbfactor = velocity*integrationOuterNormal/nbvolume;

          if (factor<0)                         // inflow
          {
            update[indexi] -= c[indexj]*factor;
            update[indexj] += c[indexj]*nbfactor;
          }
          else                         // outflow
          {
            update[indexi] -= c[indexi]*factor;
            update[indexj] += c[indexi]*nbfactor;
          }
        }
      }

      // handle boundary face
      if (intersection.boundary())                               /*@\label{evh:bndry}@*/
      {
        if (factor<0)                 // inflow, apply boundary condition
          update[indexi] -= bnd(igeo.center(),t)*factor;
        else                 // outflow
          update[indexi] -= c[indexi]*factor;
      }
    }             // end all intersections             /*@\label{evh:flux1}@*/

    // compute dt restriction
    dt = std::min(dt,1.0/sumfactor);                   /*@\label{evh:dt}@*/

  }       // end grid traversal                        /*@\label{evh:loop1}@*/

  // scale dt with safety factor
  dt *= 0.99;                                          /*@\label{evh:.99}@*/

  // update the concentration vector
  for (unsigned int i=0; i<size; ++i)
    c[i] += dt*update[i];                              /*@\label{evh:updc}@*/

  return dt;
}


#ifdef USING_COREPY
template< class GridView, class M >
double evolve ( GridView &gridView, M &mapper, pybind11::array values, pybind11::function bnd, double time )
{
  pybind11::array_t< double > doubleValues( values );
  pybind11::buffer_info info = doubleValues.request( true );
  if( (info.ndim != 1) || (info.shape[ 0 ] != static_cast< std::size_t >( mapper.size() )) || (info.strides[ 0 ] != sizeof( double )) )
    throw std::invalid_argument( "values must be of shape (mapper.size(),)" );
  double *c = static_cast< double * >( info.ptr );

  typedef typename GridView::template Codim< 0 >::Entity Entity;
  typedef typename Entity::Geometry::GlobalCoordinate GlobalCoordinate;
  auto bnd_adapter = [ bnd ] ( const GlobalCoordinate &x, double t ) { return bnd( x, t ).template cast< double >(); };

  return evolve( gridView, mapper, c, bnd_adapter, time );
}
#endif // #ifdef USING_COREPY
