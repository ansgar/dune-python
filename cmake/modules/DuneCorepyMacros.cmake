if(${DUNE_COMMON_VERSION} VERSION_LESS 2.6)
  find_package(PythonInterp 2.7 REQUIRED)
  find_package(PythonLibs 2.7 REQUIRED)
else()
  dune_python_require_version("2.7")
endif()

if(NOT PYTHONINTERP_FOUND OR NOT PYTHONLIBS_FOUND)
  message(FATAL_ERROR "Module dune-corepy requires a Python (interpreter and libraries)")
endif()

include_directories("${PYTHON_INCLUDE_DIRS}")

function(add_python_targets base)
  include(DuneSymlinkOrCopy)
  foreach(file ${ARGN})
    dune_symlink_to_source_files(FILES ${file}.py)
  endforeach()
endfunction()

if(${DUNE_COMMON_VERSION} VERSION_LESS 2.6)
  if(DUNE_PYTHON_INSTALL_LOCATION STREQUAL "user")
    set(PYTHONINSTALL TRUE)
    set(PIP_FLAGS "--user")
  elseif(DUNE_PYTHON_INSTALL_LOCATION STREQUAL "system")
    set(PYTHONINSTALL TRUE)
    set(PIP_FLAGS "")
  endif()
  if(DUNE_PYTHON_INSTALL_EDITABLE)
    set(PIP_FLAGS "${PIP_FLAGS} -e")
  endif()

  if(PYTHONINSTALL)
    install(
      CODE "execute_process(COMMAND ${PYTHON_EXECUTABLE} -m pip install --upgrade --force-reinstall ${PIP_FLAGS} ${CMAKE_BINARY_DIR}/python)"
      COMPONENT python
    )
    add_custom_target(install_python COMMAND "${CMAKE_COMMAND}" -DCMAKE_INSTALL_COMPONENT=python -P "${CMAKE_BINARY_DIR}/cmake_install.cmake")
  endif()
endif()

include(DuneAddPybind11Module)
