from __future__ import absolute_import, division, print_function, unicode_literals

import importlib
import sys
import pkgutil
import importlib
import logging
import inspect

import dune.common.module
import dune.common
import dune.generator

logger = logging.getLogger(__name__)
_create_map = dict()

package = dune
prefix = package.__name__ + "."
subpackages = []
logMsg = "Importing create registries from [ "

for importer, modname, ispkg in pkgutil.iter_modules(package.__path__, prefix):
    if not ispkg:
        continue

    # can just use modname here if registry is part of __init__ file
    try:
        # Note: modname.__init__ is imported so be aware of
        # possible side effects
        module = importlib.import_module(modname)
    except ImportError as e:
        logger.debug('failed to import ' + modname + ': ' + str(e) + '.')
        continue

    try:
        moduleRegistry = module.registry.items()
        logMsg = logMsg + modname + " "
    except AttributeError:
        logger.debug('module ' + modname + ' does not provide a registry.')
        continue

    for obj, registry in moduleRegistry:
        objmap = dict()
        try:
            objmap = _create_map[obj]
        except KeyError:
            _create_map[obj] = objmap
        for key, value in registry.items():
            if key in objmap:
                raise RuntimeError('Key \'' + key + '\' registered twice for \'' + obj + '\'.')
            else:
                objmap[key] = [value,modname]

# the grids registry also provide view -
# so we will add them to the # grid to the there as well
_create_map.setdefault("view",{}).update(_create_map["grid"])

logMsg = logMsg + "]"
logger.info(logMsg)

# a helper class
class Empty:
    pass
def signatureDict(func):
    # get signature from func, we simply fill a dictonary with the name of
    # all non var argument of the function as key and containing a either
    # Empty or the default argument provided by the function signature

    if sys.version_info.major == 2:
        sig = inspect.getargspec(func)
        names=list(sig.args)
        defaults=list(sig.defaults) if sig.defaults else list()
        for i in range(len(names)-len(defaults)):
            defaults.insert(0,Empty)
        ret = dict(zip(names, defaults))
    else:
        ret = {}
        sig = inspect.signature(func)
        for p,v in sig.parameters.items():
            # we only extract positional or keyword argument (i.e. not  *args,**kwargs)
            if v.kind == v.POSITIONAL_OR_KEYWORD:
                name = v.name
                default = v.default if not v.default is v.empty else Empty
                ret.update({name:default})
    return ret

def _creatorCall(create, usedKeys, *args, **kwargs):
    # print("*********************************************")
    # print("creatorCall:",key)
    # get signature of create function to call
    signature = signatureDict(create)
    # check if any of the parameter names correspond to some creator -
    # if a creator exists for that function name and the value passed in by
    # the user for that parameter is a string, call the creator otherwise
    # use the object provided. If no creator exsits use the the value
    # provided by the user or the default value
    for name in signature:
        # special treatment of 'view'/'grid' parameter since a 'grid' is
        # also a view
        if name=='view' and not name in kwargs and 'grid' in kwargs:
            kwargs.update({"view":kwargs["grid"]})
            usedKeys.update(["grid"])
        # print("checking parameter: ",name,end=" -\n ")
        creator = globals().get(name)
        if creator: # a creator for this parameter name exists
            assert signature[name] == Empty, "argument in create method corresponding to creatibles should not have default values"
            argument = kwargs.get(name, Empty)
            assert not argument == Empty, "required creatable argument " + argument + " not provided"
            if isinstance(argument,str):
                # recursion
                paramCreator = creator.registry[argument][0]
                signature[name] = _creatorCall(paramCreator, usedKeys, *args,**kwargs)
                kwargs[name] = signature[name] # replace the string with the actual object
            else:
                signature[name] = argument # store the object provided
            usedKeys.update([name])
        else: # no creator available
            argument = kwargs.get(name, Empty)
            if argument == Empty:
                assert not signature[name] == Empty, "no value for argument " + name + " provided"
                kwargs[name] = argument
            else:
                signature[name] = argument
                usedKeys.update([name])
    # print(signature)
    # print("*********************************************")
    return create(**signature)

def creatorCall(self, key, *args, **kwargs):
    try:
        create = self.registry[key][0]
    except KeyError:
        raise RuntimeError('No ' + self.obj + ' implementation: ' + key + '.')
    # the complex creation mechanism is only allowed with named arguemtns
    # if positional arguments have been used, call the original function directly
    # without further checking the parameters
    if len(args)>0:
        return create(*args, **kwargs)
    else:
        usedKeys = set()
        # make a fix here for grids/views
        if 'grid' in kwargs and not self.obj == 'view' and not 'view' in kwargs:
            kwargs.update({'view':kwargs['grid']})
            usedKeys.update(['grid'])
        instance = _creatorCall(create,usedKeys,*args,**kwargs)
        # print(set(kwargs), usedKeys)
        assert set(kwargs) == usedKeys, "some provided named parameters where not used"
        return instance

for obj, registry in _create_map.items():
    # docs = "\n".join(k+" from "+v[1] for k,v in registry.items())
    docs_format = "{:<25}" * (2)
    docs = docs_format.format("key", "module") + "\n"
    docs = docs + docs_format.format("----------", "----------") + "\n"
    for k,v in registry.items():
        docs = docs + docs_format.format(k,v[1]) + "\n"

    attribs = {k: staticmethod(v[0]) for k, v in registry.items()}
    attribs.update({"__call__": creatorCall, "registry": registry, "obj": obj})
    C = type(str(obj), (object,), attribs)
    c = C()
    c.__doc__ = "Create a dune grid instance, available choices are:\n"+docs
    setattr(sys.modules[__name__], obj, c)
    logger.debug("added create."+obj+" with keys: \n"+\
            "\n".join("   "+k+" from subpackage "+v[1] for k,v in registry.items()))
