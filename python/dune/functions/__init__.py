from dune.generator.generator import SimpleGenerator
from dune.common.hashit import hashIt

from .globalbasis import defaultGlobalBasis
from .tree import *

registry = dict()
registry["globalBasis"] = {
        "default" : defaultGlobalBasis
    }

generator = SimpleGenerator("GlobalBasis", "Dune::CorePy")

def load(includes, typeName, constructors=None, methods=None):
    includes = includes + ["dune/corepy/functions/globalbasis.hh"]
    moduleName = "globalbasis_" + hashIt(typeName)
    module = generator.load(includes, typeName, moduleName, constructors, methods)
    return module
